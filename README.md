# Анонимный чат

Дипломный проект на тему: "разработка веб приложерния 'Анонимный чат'"

[Пока что пустая ссылка на проект](https://google.com)

## Требования к проекту

### Нефункциональные

#### Роли

Происходит наследование возможностей ролей снизу вверх: 
админ наследует возможности  модератора, модератор пользователя

|   Название    |                                         Описание                                         |
| ------------- | ---------------------------------------------------------------------------------------- |
| Пользователь  | Неподсредственный пользователь, который использует основные функции системы              |


#### Железо

| Название  |    Примечание     |
| --------- | ----------------- |
| Процессор | 1 ядро +- 3.2 ггц |
| ОЗУ       | <= 1.5 гб         |
| ПЗУ       | До 10 гб          |


#### Деплой

- Для vds можно взять beget, ztv, 
- для s3 yandex.cloud,
- для субд можно посмотреть в сторону render.com, superbase, clever-cloud,
- redis хостить на том же вдс, где и основное


### функиональные

- [ ] деплой

#### backend

- [ ] изменить способ авторизации с http basic или с jwt (хранить токены в рантайме на фронте, по куке получать их)
- [x] вход в систему как анонимный пользователь
- [x] отвечать на сообщение
- [ ] добавить heartbeat
  - [ ] в течение 30 сек отправлять сигнал, если он не доходит по какой-то причине, отключать пользователя и делать соответствующие действия
  - [ ] если пользователь в течение 15 минут не производит никаких действий, переводить его в сон
- [ ] добавить риалтайм (по принципу discord)
- [ ] написать на все интеграционные тесты
- [ ] завернуть все в докер
- [ ] добавить личку тет-а-тет с юзером, а не профилем комнаты



#### frontend

- [ ] вход в систему как анонимный пользователь
- [ ] указание никнейма
- [ ] отвечать на сообщение
- [ ] добавить heartbeat
  - [ ] в течение 30 сек отправлять сигнал, если он не доходит по какой-то причине, отключать пользователя и делать соответствующие действия
  - [ ] если пользователь в течение 15 минут не производит никаких действий, переводить его в сон
- [ ] добавить риалтайм (по принципу discord)
- [ ] написать на все интеграционные тесты
- [ ] личка