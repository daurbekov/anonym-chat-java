package anonym.chat.application.services.user;

import anonym.chat.application.mappers.UserMapper;
import anonym.chat.application.validators.user.CreateNewAuthUserValidator;
import anonym.chat.application.validators.common.IdValidator;
import anonym.chat.core.abstractions.providers.DateTimeProvider;
import anonym.chat.core.abstractions.PasswordEncoder;
import anonym.chat.core.abstractions.Validator;
import anonym.chat.core.abstractions.user.UserRepository;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.constants.errors.ModelsErrorConstants;
import anonym.chat.core.models.user.*;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.core.utils.validator.ValidationResult;
import anonym.chat.core.utils.validator.ValidatorUtils;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final DateTimeProvider dateTimeProvider;
    private final Validator validator;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(
            DateTimeProvider dateTimeProvider,
            Validator validator,
            UserMapper userMapper,
            UserRepository userRepository,
            PasswordEncoder passwordEncoder) {
        this.dateTimeProvider = dateTimeProvider;
        this.validator = validator;
        this.userMapper = userMapper;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public ResultT<UserModel> add() {
        UserModel model = new UserModel();
        model.setRole(Role.USER);
        model.setCreatedAt(dateTimeProvider.now());

        UserModel createdUser = userRepository.add(model);

        return ResultUtils.fromTData(createdUser);
    }

    @Override
    public ResultT<UserModel> addAuthUser(CreateNewAuthUser add) {
        UserModel model = userMapper.toUserModelFromCreateNewAuthUser(add);

        CreateNewAuthUserValidator validationModel = userMapper.toCreateNewAuthUserValidatorFrom(add);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserWithSameLoginResult = getByLogin(add.userAuth().login());

        if (existsUserWithSameLoginResult.isSuccess()) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.EXISTS_TYPE,
                ModelsErrorConstants.EXISTS_TITLE,
                ModelsErrorConstants.EXISTS_STATUS,
                ModelsErrorConstants.EXISTS_DESCRIPTION
            );
        }

        String passwordHash = passwordEncoder.encode(add.userAuth().password());
        model.setCreatedAt(dateTimeProvider.now());

        UserModel createdUser = userRepository.addWithPassword(model, passwordHash);

        return ResultUtils.fromTData(createdUser);
    }

    @Override
    public ResultT<UserModel> getById(long userId) {
        IdValidator validationModel = new IdValidator(userId);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        UserModel existsUser = userRepository.getById(userId);

        if (existsUser == null) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(existsUser);
    }

    @Override
    public ResultT<UserModel> getByLogin(String login) {
        UserModel existsUser = userRepository.getByLogin(login);

        if (existsUser == null) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(existsUser);
    }

    @Override
    public ResultT<UserModel> update(UpdateUser update) {
        return null;
    }

    @Override
    public ResultT<UserModel> updateAuthUser(UpdateAuthUser update) {
        return null;
    }
}
