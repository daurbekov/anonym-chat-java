package anonym.chat.application.services.room;

import anonym.chat.application.mappers.RoomProfileMapper;
import anonym.chat.application.utils.PaginationUtils;
import anonym.chat.application.validators.avatar.AvatarIdValidator;
import anonym.chat.application.validators.room.RoomIdValidator;
import anonym.chat.application.validators.room.profile.GetByIdValidator;
import anonym.chat.application.validators.room.profile.IsInstalledToRoomValidator;
import anonym.chat.application.validators.user.UserIdValidator;
import anonym.chat.application.validators.common.PageValidator;
import anonym.chat.application.validators.room.profile.NicknameValidator;
import anonym.chat.application.validators.room.profile.SetRoomProfileValidator;
import anonym.chat.core.abstractions.providers.config.PaginationConfigProvider;
import anonym.chat.core.abstractions.Validator;
import anonym.chat.core.abstractions.avatar.AvatarService;
import anonym.chat.core.abstractions.room.RoomService;
import anonym.chat.core.abstractions.room.profile.RoomProfileRepository;
import anonym.chat.core.abstractions.room.profile.RoomProfileService;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.constants.errors.CommonErrorsConstants;
import anonym.chat.core.constants.errors.ModelsErrorConstants;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.room.profile.CreateNewRoomProfile;
import anonym.chat.core.models.room.profile.IsInstalledToRoom;
import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.core.models.room.profile.SetRoomProfileToRoom;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.paginations.PaginationConfig;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.core.utils.validator.ValidationResult;
import anonym.chat.core.utils.validator.ValidatorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.util.List;

@Lazy
@Service
public class RoomProfileServiceImpl implements RoomProfileService {
    private final RoomProfileRepository roomProfileRepository;

    private final PaginationConfigProvider paginationConfigProvider;
    private final RoomProfileMapper roomProfileMapper;
    private final AvatarService avatarService;
    private final Validator validator;
    private final UserService userService;

    @Autowired
    private RoomService roomService;


    public RoomProfileServiceImpl(
            RoomProfileRepository roomProfileRepository,
            PaginationConfigProvider paginationConfigProvider,
            RoomProfileMapper roomProfileMapper,
            AvatarService avatarService,
            Validator validator,
            UserService userService) {
        this.roomProfileRepository = roomProfileRepository;
        this.paginationConfigProvider = paginationConfigProvider;
        this.roomProfileMapper = roomProfileMapper;
        this.avatarService = avatarService;
        this.validator = validator;
        this.userService = userService;
    }

    @Override
    public ResultT<RoomProfileModel> add(CreateNewRoomProfile create) {
        UserIdValidator userIdValidationModel = new UserIdValidator(create.userId());

        ValidationResult userIdValidationResult = validator.validate(userIdValidationModel);

        if (!userIdValidationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(userIdValidationResult.getErrors());
        }

        if (create.avatarId() != null) {
            AvatarIdValidator avatarIdValidationModel = new AvatarIdValidator(create.avatarId());

            ValidationResult avatarIdValidationResult = validator.validate(avatarIdValidationModel);

            if (!avatarIdValidationResult.isValid()) {
                return ValidatorUtils.validationErrorFromErrors(avatarIdValidationResult.getErrors());
            }
        }

        NicknameValidator nicknameValidationModel = new NicknameValidator(create.nickname());

        ValidationResult nicknameValidationResult = validator.validate(nicknameValidationModel);

        if (!nicknameValidationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(nicknameValidationResult.getErrors());
        }

        ResultT<AvatarModel> avatarResult;

        if (create.avatarId() == null) {
            avatarResult = avatarService.getDefaultAvatar();
        } else {
            avatarResult = avatarService.getOfUserById(create.avatarId(), create.userId());
        }


        if (avatarResult.isFailure()) {
            return ResultUtils.fromTSingleError(avatarResult.getError());
        }

        RoomProfileModel roomProfileModel = roomProfileMapper.toRoomProfileModelFromCreateNewRoomProfile(create);

        RoomProfileModel savedRoomProfile = roomProfileRepository.add(roomProfileModel);

        if (savedRoomProfile == null) {
            return ResultUtils.fromTSingleError(
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_TYPE,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_TITLE,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_STATUS,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(savedRoomProfile);
    }

    @Override
    public ResultT<SectionCollection<RoomProfileModel>> getAllByUserIdInPage(long userId, long page) {
        UserIdValidator userIdValidationModel = new UserIdValidator(userId);

        ValidationResult userIdValidationResult = validator.validate(userIdValidationModel);

        if (!userIdValidationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(userIdValidationResult.getErrors());
        }

        PageValidator pageValidationModel = new PageValidator(page);

        ValidationResult pageValidationResult = validator.validate(pageValidationModel);

        if (!pageValidationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(pageValidationResult.getErrors());
        }

        PaginationConfig paginationConfig = paginationConfigProvider.get();

        List<RoomProfileModel> list = roomProfileRepository.getAllByUserIdInSection(
                userId,
                PaginationUtils.getOffset(page, paginationConfigProvider),
                paginationConfig.perPageDefault()
        );

        long totalByUserId = roomProfileRepository.getTotalByUserId(userId);

        SectionCollection<RoomProfileModel> section = new SectionCollection<>(
                page,
                PaginationUtils.getLastSection(totalByUserId, paginationConfigProvider),
                paginationConfig.perPageDefault(),
                list.size(),
                list);

        return ResultUtils.fromTData(section);
    }

    @Override
    public ResultT<RoomProfileModel> getById(long id, long userIdTarget) {
        GetByIdValidator validationModel = new GetByIdValidator(id, userIdTarget);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        RoomProfileModel existsRoomProfile = roomProfileRepository.getById(id, userIdTarget);

        if (existsRoomProfile == null) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(existsRoomProfile);
    }

    @Override
    public ResultT<Boolean> setRoomProfileToRoom(SetRoomProfileToRoom set) {
        SetRoomProfileValidator validationModel = roomProfileMapper
                .toSetRoomProfileValidatorFromSetRoomProfileToRoom(set);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RoomModel> existsRoomResult = roomService.getById(set.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<RoomProfileModel> existsRoomProfileResult = getById(set.roomProfileId(), set.userIdTarget());

        if (existsRoomProfileResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomProfileResult.getError());
        }

        boolean result = roomProfileRepository
                .setRoomProfileToRoom(set.roomProfileId(), set.roomId(), set.userIdTarget());

        return ResultUtils.fromTData(result);
    }

    @Override
    public ResultT<RoomProfileModel> getByRoomIdAndUserId(long roomId, long userId) {
        RoomIdValidator roomIdValidationModel = new RoomIdValidator(roomId);
        UserIdValidator userIdValidationModel = new UserIdValidator(userId);

        ValidationResult validationResult = validator.validate(roomIdValidationModel, userIdValidationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RoomModel> existsRoomResult = roomService.getById(roomId);

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<UserModel> existsUserResult = userService.getById(userId);

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        RoomProfileModel existsRoomProfile = roomProfileRepository.getByRoomIdAndUserId(roomId, userId);

        if (existsRoomProfile == null) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(existsRoomProfile);
    }

    @Override
    public ResultT<Boolean> isInstalledToRoom(IsInstalledToRoom isInstalled) {
        IsInstalledToRoomValidator validationModel = roomProfileMapper
                .toIsInstalledToRoomValidatorFromModel(isInstalled);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<RoomModel> existsRoomResult = roomService.getById(isInstalled.roomId());

        if (existsRoomResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomResult.getError());
        }

        ResultT<UserModel> existsUserResult = userService.getById(isInstalled.userId());

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        ResultT<RoomProfileModel> existsRoomProfileResult = getById(isInstalled.roomProfileId(), isInstalled.userId());

        if (existsRoomProfileResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsRoomProfileResult.getError());
        }

        RoomProfileModel existsRoomProfile = roomProfileRepository.getInstalled(
                isInstalled.roomId(),
                isInstalled.roomProfileId(),
                isInstalled.userId());

        boolean isInstalledBoolean = existsRoomProfile != null;

        return ResultUtils.fromTData(isInstalledBoolean);
    }
}
