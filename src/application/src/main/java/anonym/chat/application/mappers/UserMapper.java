package anonym.chat.application.mappers;

import anonym.chat.application.validators.user.CreateNewAuthUserValidator;
import anonym.chat.core.models.user.CreateNewAuthUser;
import anonym.chat.core.models.user.UserModel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "UserMapperImplApplication")
public interface UserMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "login", source = "userAuth.login")
    UserModel toUserModelFromCreateNewAuthUser(CreateNewAuthUser createNewAuthUser);

    @Mapping(target = "login", source = "userAuth.login")
    @Mapping(target = "password", source = "userAuth.password")
    CreateNewAuthUserValidator toCreateNewAuthUserValidatorFrom(CreateNewAuthUser createNewAuthUser);
}
