package anonym.chat.application.mappers;

import anonym.chat.application.validators.token.refresh.CheckTokenIsValidValidator;
import anonym.chat.application.validators.token.refresh.CreateNewFromExistingTokenValidator;
import anonym.chat.core.models.token.refresh.CheckTokenIsValid;
import anonym.chat.core.models.token.refresh.CreateNewFromExistingToken;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        implementationName = "RefreshTokenMapperImplApplication")
public interface RefreshTokenMapper {
    CreateNewFromExistingTokenValidator toCreateNewFromExistingTokenValidatorFromCreateNewFromExistingToken(
            CreateNewFromExistingToken source);

    CheckTokenIsValidValidator toCheckTokenIsValidValidatorFromCheckTokenIsValid(CheckTokenIsValid source);
}
