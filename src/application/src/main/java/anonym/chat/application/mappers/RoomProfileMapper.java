package anonym.chat.application.mappers;

import anonym.chat.application.validators.room.profile.IsInstalledToRoomValidator;
import anonym.chat.application.validators.room.profile.SetRoomProfileValidator;
import anonym.chat.core.models.room.profile.CreateNewRoomProfile;
import anonym.chat.core.models.room.profile.IsInstalledToRoom;
import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.core.models.room.profile.SetRoomProfileToRoom;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "RoomProfileMapperImplApplication")
public interface RoomProfileMapper {
    @Mapping(target = "id", ignore = true)
    RoomProfileModel toRoomProfileModelFromCreateNewRoomProfile(CreateNewRoomProfile source);
    SetRoomProfileValidator toSetRoomProfileValidatorFromSetRoomProfileToRoom(SetRoomProfileToRoom source);
    IsInstalledToRoomValidator toIsInstalledToRoomValidatorFromModel(IsInstalledToRoom source);
}
