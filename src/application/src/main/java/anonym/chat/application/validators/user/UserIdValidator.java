package anonym.chat.application.validators.user;

import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Min;

public record UserIdValidator(
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long userId
) implements ValidationModel {
}
