package anonym.chat.application.validators.room.profile;

import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Min;

public record GetByIdValidator(
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long id,

    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long userIdTarget
) implements ValidationModel { }
