package anonym.chat.application.services.avatar;

import anonym.chat.application.mappers.AvatarMapper;
import anonym.chat.application.utils.PaginationUtils;
import anonym.chat.application.validators.avatar.CreateNewAvatarValidator;
import anonym.chat.application.validators.avatar.GetAllOfUserSectionValidator;
import anonym.chat.application.validators.common.IdValidator;
import anonym.chat.application.validators.user.UserIdValidator;
import anonym.chat.core.abstractions.providers.config.PaginationConfigProvider;
import anonym.chat.core.abstractions.Validator;
import anonym.chat.core.abstractions.avatar.AvatarEditor;
import anonym.chat.core.abstractions.avatar.AvatarRepository;
import anonym.chat.core.abstractions.avatar.AvatarService;
import anonym.chat.core.abstractions.file.FileService;
import anonym.chat.core.abstractions.file.temp.FileTempStorage;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.constants.errors.CommonErrorsConstants;
import anonym.chat.core.constants.errors.ModelsErrorConstants;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.core.models.avatar.CreateNewAvatar;
import anonym.chat.core.models.avatar.EditAvatar;
import anonym.chat.core.models.avatar.GetAllOfUserSection;
import anonym.chat.core.models.file.FileModel;
import anonym.chat.core.models.file.temp.FileTempModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.paginations.PaginationConfig;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.core.utils.validator.ValidationResult;
import anonym.chat.core.utils.validator.ValidatorUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AvatarServiceImpl implements AvatarService {
    private final AvatarRepository avatarRepository;
    private final AvatarEditor avatarEditor;

    private final FileTempStorage fileTempStorage;
    private final FileService fileService;
    private final Validator validator;
    private final AvatarMapper avatarMapper;
    private final UserService userService;
    private final PaginationConfigProvider paginationConfigProvider;

    public AvatarServiceImpl(
            AvatarRepository avatarRepository,
            AvatarEditor avatarEditor,
            FileTempStorage fileTempStorage,
            FileService fileService,
            Validator validator,
            AvatarMapper avatarMapper,
            UserService userService,
            PaginationConfigProvider paginationConfigProvider) {
        this.avatarRepository = avatarRepository;
        this.avatarEditor = avatarEditor;
        this.fileTempStorage = fileTempStorage;
        this.fileService = fileService;
        this.validator = validator;
        this.avatarMapper = avatarMapper;
        this.userService = userService;
        this.paginationConfigProvider = paginationConfigProvider;
    }

    @Override
    public ResultT<AvatarModel> add(CreateNewAvatar createNewAvatar) {
        CreateNewAvatarValidator validationModel = avatarMapper
                .toCreateNewAvatarValidatorFromCreateNewAvatar(createNewAvatar);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        Optional<FileTempModel> savedTempFile = fileTempStorage.saveTempFileByStream(createNewAvatar.file());

        if (savedTempFile.isEmpty()) {
            return ResultUtils.fromTSingleError(
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_TYPE,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_TITLE,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_STATUS,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_DESCRIPTION
            );
        }

        EditAvatar forEdit = avatarMapper.toEditAvatarFromCreateNewAvatar(createNewAvatar, savedTempFile.get());

        FileTempModel editedAvatar = avatarEditor.editAvatar(forEdit);

        ResultT<FileModel> savedFileResult = fileService.addExistsFile(editedAvatar);

        fileTempStorage.deleteTempFile(savedTempFile.get().getFile());
        fileTempStorage.deleteTempFile(editedAvatar.getFile());

        if (savedFileResult.isFailure()) {
            return ResultUtils.fromTSingleError(savedFileResult.getError());
        }

        FileModel savedFile = savedFileResult.getData();

        AvatarModel savedAvatar = avatarRepository.add(createNewAvatar.userId(), savedFile.getId());

        if (savedAvatar == null) {
            return ResultUtils.fromTSingleError(
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_TYPE,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_TITLE,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_STATUS,
                CommonErrorsConstants.INTERNAL_SERVER_ERROR_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(savedAvatar);
    }

    @Override
    public ResultT<AvatarModel> getOfUserById(long id, long userId) {
        IdValidator idValidationModel = new IdValidator(id);
        UserIdValidator userIdValidator = new UserIdValidator(userId);

        ValidationResult validationResult = validator.validate(idValidationModel, userIdValidator);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult = userService.getById(userId);

        // TODO: снова типизировать ошибки
        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        AvatarModel existsAvatar = avatarRepository.getOfUserById(id, userId);

        if (existsAvatar == null) {
            return ResultUtils.fromTSingleError(
                ModelsErrorConstants.NOT_EXISTS_TYPE,
                ModelsErrorConstants.NOT_EXISTS_TITLE,
                ModelsErrorConstants.NOT_EXISTS_STATUS,
                ModelsErrorConstants.NOT_EXISTS_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(existsAvatar);
    }

    @Override
    public ResultT<AvatarModel> getDefaultAvatar() {
        return null;
    }

    @Override
    public ResultT<SectionCollection<AvatarModel>> getAllOfUserSection(GetAllOfUserSection get) {
        GetAllOfUserSectionValidator validationModel = avatarMapper.toGetAllOfUserSectionValidatorFromModel(get);

        ValidationResult validationResult = validator.validate(validationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        PaginationConfig paginationConfig = paginationConfigProvider.get();

        ResultT<UserModel> existsUserResult = userService.getById(get.userId());

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        List<AvatarModel> sectionList = avatarRepository.getAllOfUserSection(
            get.userId(),
            PaginationUtils.getOffset(get.section(), paginationConfigProvider),
            paginationConfig.perPageDefault()
        );

        long lastSection = avatarRepository.getTotalOfUser(get.userId());

        SectionCollection<AvatarModel> section = new SectionCollection<>(
            get.section(),
            PaginationUtils.getLastSection(lastSection, paginationConfigProvider),
            paginationConfig.perPageDefault(),
            sectionList.size(),
            sectionList
        );

        return ResultUtils.fromTData(section);
    }
}
