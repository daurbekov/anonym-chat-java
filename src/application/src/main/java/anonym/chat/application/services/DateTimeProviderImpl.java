package anonym.chat.application.services;

import anonym.chat.core.abstractions.providers.DateTimeProvider;
import org.springframework.stereotype.Service;

import java.time.Clock;
import java.time.LocalDateTime;

@Service
public class DateTimeProviderImpl implements DateTimeProvider {
    @Override
    public LocalDateTime now() {
        return LocalDateTime.now(Clock.systemDefaultZone());
    }

    @Override
    public LocalDateTime nowUTC() {
        return LocalDateTime.now(Clock.systemUTC());
    }
}
