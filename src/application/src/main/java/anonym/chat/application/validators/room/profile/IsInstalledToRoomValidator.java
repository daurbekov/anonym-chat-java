package anonym.chat.application.validators.room.profile;

import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Min;

public record IsInstalledToRoomValidator(
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long roomId,

    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long roomProfileId,

    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long userId
) implements ValidationModel { }
