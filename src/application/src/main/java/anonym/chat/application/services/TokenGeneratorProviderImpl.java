package anonym.chat.application.services;

import anonym.chat.core.abstractions.PasswordEncoder;
import anonym.chat.core.abstractions.providers.TokenGeneratorProvider;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class TokenGeneratorProviderImpl implements TokenGeneratorProvider {
    private final PasswordEncoder encoder;

    public TokenGeneratorProviderImpl(PasswordEncoder encoder) {
        this.encoder = encoder;
    }

    @Override
    public String generate() {
        String uuid = UUID.randomUUID().toString();

        return encoder.encode(uuid);
    }
}
