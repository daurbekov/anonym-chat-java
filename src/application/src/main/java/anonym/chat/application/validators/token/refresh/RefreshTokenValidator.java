package anonym.chat.application.validators.token.refresh;

import anonym.chat.core.constants.validations.CommonValidationMessages;
import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;

public record RefreshTokenValidator(
    @NotEmpty(message = CommonValidationMessages.FIELD_EMPTY)
    @Length(min = 30, message = "Длина строки не соответствует ожидаемым параметрам")
    String refreshToken
) implements ValidationModel { }
