package anonym.chat.application.utils;

import anonym.chat.core.abstractions.providers.config.PaginationConfigProvider;
import anonym.chat.core.utils.paginations.PaginationConfig;

public final class PaginationUtils {
    public static long getOffset(long section, PaginationConfigProvider configProvider) {
        section = section > 0 ? section - 1 : 0;

        PaginationConfig paginationConfig = configProvider.get();

        return section * paginationConfig.perPageDefault();
    }

    public static long getLastSection(long total, PaginationConfigProvider configProvider) {
        total = total > 0 ? total : 0;

        PaginationConfig paginationConfig = configProvider.get();

        long lastSection = (long)Math.floor(((double) total) / ((double) paginationConfig.perPageDefault()));
        long lastSectionRemainder = total % paginationConfig.perPageDefault() > 0 ? 1 : 0;

        return Math.max(1, lastSection + lastSectionRemainder);
    }
}
