package anonym.chat.application.validators.message;

import anonym.chat.core.models.ValidationModel;
import jakarta.validation.constraints.Min;

public record GetAllPrivateMessageDestinationUserSectionValidator(
    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long roomId,

    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long userIdForPrivates,

    @Min(value = 1, message = "Значение не должно быть меньше, чем 1")
    long section
) implements ValidationModel { }
