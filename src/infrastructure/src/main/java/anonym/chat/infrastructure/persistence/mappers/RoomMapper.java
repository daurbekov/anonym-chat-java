package anonym.chat.infrastructure.persistence.mappers;

import anonym.chat.core.models.room.RoomModel;
import anonym.chat.infrastructure.persistence.models.room.Room;
import anonym.chat.infrastructure.persistence.models.user.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring", implementationName = "RoomMapperImplInfrastructure")
public interface RoomMapper {

    @Mapping(target = "id", source = "roomModel.id")
    @Mapping(target = "user", source = "user")
    @Mapping(target = "name", source = "roomModel.name")
    @Mapping(target = "description", source = "roomModel.description")
    @Mapping(target = "createdAt", source = "roomModel.createdAt")
    Room toRoomFromRoomModelAndUser(RoomModel roomModel, User user);

    @Mapping(target = "userId", source = "user.id")
    RoomModel toRoomModelFromRoom(Room room);

    default List<RoomModel> toRoomModelListFromRoomList(List<Room> roomList) {
        return roomList.stream().map(this::toRoomModelFromRoom).toList();
    }
}
