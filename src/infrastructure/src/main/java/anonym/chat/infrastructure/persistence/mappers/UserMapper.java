package anonym.chat.infrastructure.persistence.mappers;

import anonym.chat.core.models.user.UserModel;
import anonym.chat.infrastructure.persistence.models.user.User;
import anonym.chat.infrastructure.persistence.models.user.UserAuth;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring", implementationName = "UserMapperImplInfrastructure")
public interface UserMapper {
    @Mapping(target = "role", expression = "java(target.getRole().getValue())")
    User from(UserModel target);

    @Mapping(target = "role", expression = "java(anonym.chat.core.models.user.Role.fromInt(target.getRole()))")
    @Mapping(target = "login", ignore = true)
    UserModel from(User target);

    @Mapping(target = "role", expression = "java(anonym.chat.core.models.user.Role.fromInt(user.getRole()))")
    @Mapping(target = "login", source = "login")
    UserModel fromWithLogin(User user, String login);

    @Mapping(target = "user", source = "user")
    @Mapping(target = "login", source = "login")
    @Mapping(target = "password", source = "password")
    UserAuth fromWithAuth(User user, String login, String password);

    default List<UserModel> toUserModelListFromUserList(List<User> source) {
        return source.stream().map(this::from).toList();
    }
}
