package anonym.chat.infrastructure.persistence.mappers;

import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.infrastructure.persistence.models.avatar.Avatar;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", implementationName = "AvatarMapperImplInfrastructure")
public interface AvatarMapper {

    @Mapping(target = "userId", source = "avatar.user.id")
    @Mapping(target = "fileId", source = "avatar.file.id")
    AvatarModel toAvatarModelFromAvatar(Avatar avatar);
}
