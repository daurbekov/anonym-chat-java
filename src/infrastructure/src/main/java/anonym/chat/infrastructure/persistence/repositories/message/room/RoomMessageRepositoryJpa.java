package anonym.chat.infrastructure.persistence.repositories.message.room;

import anonym.chat.infrastructure.persistence.models.message.room.RoomMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomMessageRepositoryJpa extends JpaRepository<RoomMessage, Long> {
    @Query(value =
            "SELECT rm.* FROM room_messages rm " +
            "LEFT JOIN room_private_messages rpm ON rpm.room_message_id = rm.id " +
            "INNER JOIN messages m ON m.id = rm.message_id " +
            "INNER JOIN users u ON u.id = m.user_id " +
            "WHERE rm.room_id = ?1 " +
            " AND ( ( rpm.user_id = ?2 ) " +
            "  OR ( rm.id = rpm.room_message_id AND m.user_id = ?2 ) " +
            "  OR ( rm.id != rpm.room_message_id OR rpm.room_message_id IS NULL ) ) " +
            "LIMIT ?4 OFFSET ?3",
            nativeQuery = true)
    List<RoomMessage> getAllInRoomSection(long roomId, long userIdForPrivates, long offset, long count);

    @Query(
            value = "SELECT count(rm.*) FROM room_messages rm " +
                    "LEFT JOIN room_private_messages rpm ON rpm.room_message_id = rm.id " +
                    "INNER JOIN messages m ON m.id = rm.message_id " +
                    "INNER JOIN users u ON u.id = m.user_id " +
                    "WHERE rm.room_id = ?1 " +
                    " AND ( ( rpm.user_id = ?2 ) " +
                    "  OR ( rm.id = rpm.room_message_id AND m.user_id = ?2 ) " +
                    "  OR ( rm.id != rpm.room_message_id OR rpm.room_message_id IS NULL ) )",
            nativeQuery = true)
    Long getTotalInRoom(long roomId, long userIdForPrivates);

    @Query(value =
            "SELECT rm.* FROM room_messages rm " +
            "LEFT JOIN room_private_messages rpm ON rpm.room_message_id = rm.id " +
            "INNER JOIN messages m ON m.id = rm.message_id " +
            "INNER JOIN users u ON u.id = m.user_id " +
            "WHERE rm.room_id = ?1 AND rm.message_id = ?3" +
            " AND ( ( rpm.user_id = ?2 ) " +
            "  OR ( rm.id = rpm.room_message_id AND m.user_id = ?2 ) " +
            "  OR ( rm.id != rpm.room_message_id OR rpm.room_message_id IS NULL ) )",
            nativeQuery = true)
    Optional<RoomMessage> getByIdInRoom(long roomId, long userIdForPrivates, long id);

    @Query(
            value = "SELECT rm.* FROM room_messages rm " +
                    "LEFT JOIN room_private_messages rpm ON rpm.room_message_id IS NULL " +
                    "WHERE rm.id = ?1",
            nativeQuery = true)
    Optional<RoomMessage> getByIdInRoomPublic(long roomId, long id);
}
