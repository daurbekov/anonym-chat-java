package anonym.chat.infrastructure.persistence.models.avatar;

import anonym.chat.infrastructure.persistence.models.file.File;
import anonym.chat.infrastructure.persistence.models.user.User;
import jakarta.persistence.*;

@Entity
@Table(name = "avatars", schema = "public")
public class Avatar {
    @Id
    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User user;


    @OneToOne
    @JoinColumn(name = "file_id", referencedColumnName = "id")
    private File file;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
}
