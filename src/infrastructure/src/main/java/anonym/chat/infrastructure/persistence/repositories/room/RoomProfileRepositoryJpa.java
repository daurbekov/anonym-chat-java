package anonym.chat.infrastructure.persistence.repositories.room;

import anonym.chat.infrastructure.persistence.models.room.RoomProfile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface RoomProfileRepositoryJpa extends JpaRepository<RoomProfile, Long> {
    @Query(value = "SELECT * FROM room_profiles WHERE user_id = ?1 LIMIT ?3 OFFSET ?2", nativeQuery = true)
    List<RoomProfile> getAllByUserIdInSection(long userId, long offset, long count);

    @Query(
            value = "SELECT count(*) FROM room_profiles WHERE user_id = ?1",
            nativeQuery = true)
    Long getTotalByUserId(long userId);

    @Query(value =
            "SELECT rp.* FROM room_profiles rp " +
            "INNER JOIN selected_room_profiles srp ON srp.room_profile_id = rp.id " +
            "INNER JOIN rooms r ON srp.room_id = r.id " +
            "WHERE r.id = ?1 AND rp.user_id = ?2",
            nativeQuery = true)
    RoomProfile getByRoomIdAndUserId(long roomId, long userId);

    @Query(value =
            "SELECT rp.* FROM room_profiles rp " +
            "INNER JOIN users u ON u.id = rp.user_id " +
            "WHERE u.id = ?1 AND rp.id = ?2",
            nativeQuery = true)
    Optional<RoomProfile> getByUserIdTargetAndProfileId(long userId, long profileId);

    @Query(
            value = "SELECT rp.* FROM room_profiles rp " +
                    "INNER JOIN selected_room_profiles srp ON srp.room_profile_id = rp.id " +
                    "INNER JOIN rooms r ON srp.room_id = r.id " +
                    "INNER JOIN users u ON u.id = rp.user_id " +
                    "WHERE u.id = ?1 AND rp.id = ?2 AND r.id = ?3",
            nativeQuery = true)
    Optional<RoomProfile> getInstalled(long userId, long profileId, long roomId);
}
