package anonym.chat.infrastructure.persistence.repositories.token.refresh;

import anonym.chat.core.abstractions.token.refresh.RefreshSessionRepository;
import anonym.chat.core.models.token.refresh.RefreshSessionModel;
import anonym.chat.infrastructure.persistence.mappers.RefreshSessionMapper;
import anonym.chat.infrastructure.persistence.models.token.refresh.RefreshSession;
import anonym.chat.infrastructure.persistence.models.user.User;
import anonym.chat.infrastructure.persistence.repositories.user.UserRepositoryJpa;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Optional;

@Repository
public class RefreshSessionRepositoryImpl implements RefreshSessionRepository {
    private final RefreshSessionRepositoryJpa refreshSessionRepositoryJpa;

    private final RefreshSessionMapper refreshSessionMapper;
    private final UserRepositoryJpa userRepositoryJpa;

    public RefreshSessionRepositoryImpl(
            RefreshSessionRepositoryJpa refreshSessionRepositoryJpa,
            RefreshSessionMapper refreshSessionMapper,
            UserRepositoryJpa userRepositoryJpa) {
        this.refreshSessionRepositoryJpa = refreshSessionRepositoryJpa;
        this.refreshSessionMapper = refreshSessionMapper;
        this.userRepositoryJpa = userRepositoryJpa;
    }

    @Override
    public RefreshSessionModel add(long userId, String token, LocalDateTime createdAt, LocalDateTime expiresIn) {
        User existsUser = userRepositoryJpa.getById(userId);

        RefreshSession newRefreshSession = new RefreshSession();
        newRefreshSession.setUser(existsUser);
        newRefreshSession.setRefreshToken(token);
        newRefreshSession.setCreatedAt(createdAt);
        newRefreshSession.setExpiresIn(expiresIn);

        RefreshSession savedRefreshSession = refreshSessionRepositoryJpa.save(newRefreshSession);

        return refreshSessionMapper.toModelFromEntity(savedRefreshSession);
    }

    @Override
    public RefreshSessionModel getByToken(String refreshToken) {
        Optional<RefreshSession> existsRefreshSession = refreshSessionRepositoryJpa
                .findByToken(refreshToken);

        return existsRefreshSession
                .map(refreshSessionMapper::toModelFromEntity)
                .orElse(null);
    }

    @Override
    public boolean deleteByRefreshToken(String refreshToken) {
        Optional<RefreshSession> existsRefreshSession = refreshSessionRepositoryJpa
                .findByToken(refreshToken);

        if (existsRefreshSession.isEmpty()) return false;

        refreshSessionRepositoryJpa.delete(existsRefreshSession.get());

        return true;
    }
}
