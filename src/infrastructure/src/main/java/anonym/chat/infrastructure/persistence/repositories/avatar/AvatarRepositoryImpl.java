package anonym.chat.infrastructure.persistence.repositories.avatar;

import anonym.chat.core.abstractions.avatar.AvatarRepository;
import anonym.chat.core.abstractions.file.FileService;
import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.infrastructure.persistence.mappers.AvatarMapper;
import anonym.chat.infrastructure.persistence.models.avatar.Avatar;
import anonym.chat.infrastructure.persistence.models.file.File;
import anonym.chat.infrastructure.persistence.models.user.User;
import anonym.chat.infrastructure.persistence.repositories.file.FileRepositoryJpa;
import anonym.chat.infrastructure.persistence.repositories.user.UserRepositoryJpa;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class AvatarRepositoryImpl implements AvatarRepository {
    private final AvatarRepositoryJpa avatarRepositoryJpa;

    private final FileRepositoryJpa fileRepositoryJpa;
    private final UserRepositoryJpa userRepositoryJpa;
    private final AvatarMapper avatarMapper;

    public AvatarRepositoryImpl(
            AvatarRepositoryJpa avatarRepositoryJpa,
            FileRepositoryJpa fileRepositoryJpa,
            UserRepositoryJpa userRepositoryJpa,
            AvatarMapper avatarMapper) {
        this.avatarRepositoryJpa = avatarRepositoryJpa;
        this.fileRepositoryJpa = fileRepositoryJpa;
        this.userRepositoryJpa = userRepositoryJpa;
        this.avatarMapper = avatarMapper;
    }

    @Override
    public AvatarModel add(long userId, long fileId) {
        Optional<File> existsFile = fileRepositoryJpa.findById(fileId);
        Optional<User> existsUser = userRepositoryJpa.findById(userId);

        if (existsFile.isEmpty()) return null;
        if (existsUser.isEmpty()) return null;

        Avatar entity = new Avatar();

        entity.setFile(existsFile.get());
        entity.setUser(existsUser.get());

        Avatar savedAvatar = avatarRepositoryJpa.save(entity);

        return avatarMapper.toAvatarModelFromAvatar(savedAvatar);
    }

    @Override
    public AvatarModel getOfUserById(long id, long userId) {
        Optional<Avatar> existsAvatar = avatarRepositoryJpa.getOfUserById(id, userId);

        return existsAvatar.map(avatarMapper::toAvatarModelFromAvatar).orElse(null);
    }

    @Override
    public List<AvatarModel> getAllOfUserSection(long userId, long offset, long count) {
        return avatarRepositoryJpa.getAllOfUserSection(userId, offset, count)
                .stream().map(avatarMapper::toAvatarModelFromAvatar)
                .toList();
    }

    @Override
    public long getTotalOfUser(long userId) {
        return avatarRepositoryJpa.getTotalOfUser(userId);
    }
}
