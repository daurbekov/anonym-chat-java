package anonym.chat.infrastructure.persistence.repositories.user;

import anonym.chat.infrastructure.persistence.models.user.UserAuth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAuthRepositoryJpa extends JpaRepository<UserAuth, Long> {
    @Query(value = "SELECT au.* FROM user_auth au WHERE au.user_Id = :id", nativeQuery = true)
    UserAuth getById(@Param("id") long id);

    @Query(value = "SELECT password FROM user_auth WHERE user_id = :user_id", nativeQuery = true)
    String getPasswordHashByUserId(@Param("user_id") Long userId);
}
