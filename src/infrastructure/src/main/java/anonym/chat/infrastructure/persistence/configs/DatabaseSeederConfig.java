package anonym.chat.infrastructure.persistence.configs;

import anonym.chat.infrastructure.persistence.seeders.DatabaseSeeder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DatabaseSeederConfig {
    @Value("${database.seeder.enable}")
    private boolean enable;

    @Value("${database.seeder.users.admin.login}")
    private String adminLogin;

    @Value("${database.seeder.users.admin.password}")
    private String adminPassword;

    @Value("${database.seeder.rooms.main.name}")
    private String mainRoomName;

    @Value("${database.seeder.rooms.main.description}")
    private String mainRoomDescription;

    public String getAdminLogin() {
        return adminLogin;
    }

    public String getAdminPassword() {
        return adminPassword;
    }

    public String getMainRoomName() {
        return mainRoomName;
    }

    public String getMainRoomDescription() {
        return mainRoomDescription;
    }

    @Bean
    public CommandLineRunner seed(DatabaseSeeder databaseSeeder) {
        if (!enable) return (String... args) -> {};

        databaseSeeder.seed();

        return (String... args) -> {};
    }
}
