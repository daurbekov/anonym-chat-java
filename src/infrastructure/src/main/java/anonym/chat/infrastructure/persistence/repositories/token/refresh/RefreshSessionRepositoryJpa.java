package anonym.chat.infrastructure.persistence.repositories.token.refresh;

import anonym.chat.infrastructure.persistence.models.token.refresh.RefreshSession;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RefreshSessionRepositoryJpa extends JpaRepository<RefreshSession, Long> {
    @Query(value = "SELECT * FROM refresh_sessions WHERE refresh_token = ?1", nativeQuery = true)
    Optional<RefreshSession> findByToken(String refreshToken);
}
