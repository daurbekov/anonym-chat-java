package anonym.chat.infrastructure.persistence.repositories.room;

import anonym.chat.core.abstractions.room.profile.RoomProfileRepository;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.infrastructure.persistence.mappers.RoomProfileMapper;
import anonym.chat.infrastructure.persistence.models.avatar.Avatar;
import anonym.chat.infrastructure.persistence.models.room.RoomProfile;
import anonym.chat.infrastructure.persistence.models.room.selected.profile.SelectedRoomProfile;
import anonym.chat.infrastructure.persistence.models.room.selected.profile.SelectedRoomProfileId;
import anonym.chat.infrastructure.persistence.models.user.User;
import anonym.chat.infrastructure.persistence.repositories.avatar.AvatarRepositoryJpa;
import anonym.chat.infrastructure.persistence.repositories.user.UserRepositoryJpa;
import org.springframework.stereotype.Repository;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Repository
public class RoomProfileRepositoryImpl implements RoomProfileRepository {
    private final RoomProfileRepositoryJpa roomProfileRepositoryJpa;
    private final SelectedRoomProfileJpa selectedRoomProfileJpa;

    private final RoomProfileMapper roomProfileMapper;
    private final UserRepositoryJpa userRepositoryJpa;
    private final AvatarRepositoryJpa avatarRepositoryJpa;

    public RoomProfileRepositoryImpl(
            RoomProfileRepositoryJpa roomProfileRepositoryJpa,
            SelectedRoomProfileJpa selectedRoomProfileJpa,
            RoomProfileMapper roomProfileMapper,
            UserRepositoryJpa userRepositoryJpa,
            AvatarRepositoryJpa avatarRepositoryJpa) {
        this.roomProfileRepositoryJpa = roomProfileRepositoryJpa;
        this.selectedRoomProfileJpa = selectedRoomProfileJpa;
        this.roomProfileMapper = roomProfileMapper;
        this.userRepositoryJpa = userRepositoryJpa;
        this.avatarRepositoryJpa = avatarRepositoryJpa;
    }

    @Override
    public RoomProfileModel add(RoomProfileModel roomProfileModel) {
        Optional<User> user = userRepositoryJpa.findById(roomProfileModel.getUserId());
        Optional<Avatar> avatar = avatarRepositoryJpa.findById(roomProfileModel.getAvatarId());

        RoomProfile entity = roomProfileMapper.toRoomProfileFromRoomProfileModel(roomProfileModel, user.get(), avatar.get());

        RoomProfile savedEntity = roomProfileRepositoryJpa.save(entity);

        return roomProfileMapper.toRoomProfileModelFromRoomProfile(savedEntity);
    }

    @Override
    public List<RoomProfileModel> getAllByUserIdInSection(long userId, long offset, long count) {
        List<RoomProfile> list = roomProfileRepositoryJpa.getAllByUserIdInSection(userId, offset, count);

        return roomProfileMapper.toRoomProfileModelListFromRoomProfileList(list);
    }

    @Override
    public long getTotalByUserId(long userId) {
        return roomProfileRepositoryJpa.getTotalByUserId(userId);
    }

    @Override
    public RoomProfileModel getById(long id, long userIdTarget) {
        Optional<RoomProfile> existsRoomProfile = roomProfileRepositoryJpa.getByUserIdTargetAndProfileId(userIdTarget, id);

        return existsRoomProfile.map(roomProfileMapper::toRoomProfileModelFromRoomProfile).orElse(null);
    }

    @Override
    public Boolean setRoomProfileToRoom(long roomProfileId, long roomId, long userIdTarget) {
        SelectedRoomProfile existsSelect = selectedRoomProfileJpa.getByUserIdAndRoomId(userIdTarget, roomId);

        SelectedRoomProfile selectedRoomProfile = new SelectedRoomProfile();
        SelectedRoomProfileId selectedRoomProfileId = new SelectedRoomProfileId();
        selectedRoomProfile.setSelectedRoomProfileId(selectedRoomProfileId);
        selectedRoomProfileId.setRoomProfileId(roomProfileId);
        selectedRoomProfileId.setRoomId(roomId);

        if (existsSelect != null) {
            int result = selectedRoomProfileJpa.updateByRoomProfileIdAndRoomId(roomProfileId, roomId, userIdTarget);

            return result != 0;
        } else {
            SelectedRoomProfile created = selectedRoomProfileJpa.save(selectedRoomProfile);

            return true;
        }
    }

    @Override
    public RoomProfileModel getByRoomIdAndUserId(long roomId, long userId) {
        RoomProfile existsRoomProfile = roomProfileRepositoryJpa.getByRoomIdAndUserId(roomId, userId);

        if (existsRoomProfile == null) return null;

        return roomProfileMapper.toRoomProfileModelFromRoomProfile(existsRoomProfile);
    }

    @Override
    public RoomProfileModel getInstalled(long roomId, long userProfileId, long userId) {
        return roomProfileRepositoryJpa.getInstalled(userId, userProfileId, roomId)
                .map(roomProfileMapper::toRoomProfileModelFromRoomProfile)
                .orElse(null);
    }
}
