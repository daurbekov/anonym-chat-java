package anonym.chat.infrastructure.persistence.repositories.message;

import anonym.chat.infrastructure.persistence.models.message.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepositoryJpa extends JpaRepository<Message, Long> {
}
