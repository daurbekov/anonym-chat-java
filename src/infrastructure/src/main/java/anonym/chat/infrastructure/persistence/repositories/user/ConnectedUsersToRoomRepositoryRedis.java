package anonym.chat.infrastructure.persistence.repositories.user;

import anonym.chat.infrastructure.persistence.models.room.ConnectedUsersToRoomRedis;
import org.springframework.data.keyvalue.repository.KeyValueRepository;
import org.springframework.data.util.Streamable;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ConnectedUsersToRoomRepositoryRedis extends KeyValueRepository<ConnectedUsersToRoomRedis, String> {
    Streamable<ConnectedUsersToRoomRedis> findAllByRoomId(long roomId);
    Optional<ConnectedUsersToRoomRedis> findByUserIdAndRoomId(long userId, long roomId);
}
