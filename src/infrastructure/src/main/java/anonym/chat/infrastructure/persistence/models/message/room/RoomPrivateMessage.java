package anonym.chat.infrastructure.persistence.models.message.room;

import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;

@Entity
@Table(name = "room_private_messages", schema = "public")
public class RoomPrivateMessage {

    @EmbeddedId
    private RoomPrivateMessageId roomPrivateMessageId;


    public RoomPrivateMessageId getRoomPrivateMessageId() {
        return roomPrivateMessageId;
    }

    public void setRoomPrivateMessageId(RoomPrivateMessageId roomPrivateMessageId) {
        this.roomPrivateMessageId = roomPrivateMessageId;
    }
}
