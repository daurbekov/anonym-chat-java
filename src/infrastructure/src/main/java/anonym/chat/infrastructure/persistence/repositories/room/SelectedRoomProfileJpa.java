package anonym.chat.infrastructure.persistence.repositories.room;

import anonym.chat.infrastructure.persistence.models.room.selected.profile.SelectedRoomProfile;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface SelectedRoomProfileJpa extends JpaRepository<SelectedRoomProfile, Long> {
    @Query(value = "SELECT * FROM selected_room_profiles WHERE room_profile_id = ?1 AND room_id = ?2", nativeQuery = true)
    SelectedRoomProfile getByRoomProfileIdAndRoomId(long roomProfileId, long roomId);

    @Query(
            value = "SELECT srp.* " +
                    "FROM selected_room_profiles srp " +
                    "INNER JOIN room_profiles rp ON srp.room_profile_id = rp.id " +
                    "INNER JOIN users u on u.id = rp.user_id " +
                    "WHERE u.id = rp.user_id AND srp.room_id = ?2 AND u.id = ?1",
            nativeQuery = true)
    SelectedRoomProfile getByUserIdAndRoomId(long userId, long roomId);

    @Transactional
    @Modifying
    @Query(value =
            "UPDATE selected_room_profiles srp " +
            "SET room_profile_id = ?1 " +
            "FROM room_profiles rp " +
            "INNER JOIN users u ON u.id = rp.user_id " +
            "WHERE srp.room_profile_id = rp.id " +
            " AND u.id = ?3 " +
            " AND srp.room_id = ?2 " +
            " AND rp.id = ?1",
            nativeQuery = true)
    int updateByRoomProfileIdAndRoomId(long roomProfileId, long roomId, long userIdTarget);
}
