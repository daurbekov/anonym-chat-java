package anonym.chat.infrastructure.persistence.repositories.user;

import anonym.chat.core.abstractions.user.UserRepository;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.infrastructure.persistence.mappers.UserMapper;
import anonym.chat.infrastructure.persistence.models.user.User;
import anonym.chat.infrastructure.persistence.models.user.UserAuth;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final UserMapper userMapper;
    private final UserRepositoryJpa userRepositoryJpa;
    private final UserAuthRepositoryJpa userAuthRepositoryJpa;

    public UserRepositoryImpl(UserMapper userMapper, UserRepositoryJpa userRepositoryJpa, UserAuthRepositoryJpa userAuthRepositoryJpa) {
        this.userMapper = userMapper;
        this.userRepositoryJpa = userRepositoryJpa;
        this.userAuthRepositoryJpa = userAuthRepositoryJpa;
    }

    @Override
    public UserModel add(UserModel userModel) {
        User userEntity = userMapper.from(userModel);

        var createdUserEntity = userRepositoryJpa.save(userEntity);

        return userMapper.from(createdUserEntity);
    }

    @Transactional
    @Override
    public UserModel addWithPassword(UserModel userModel, String password) {
        User userEntity = userMapper.from(userModel);

        var createdUserEntity = userRepositoryJpa.save(userEntity);

        UserAuth userAuth = userMapper.fromWithAuth(userEntity, userModel.getLogin(), password);

        var createdUserAuthEntity = userAuthRepositoryJpa.save(userAuth);

        return userMapper.fromWithLogin(createdUserEntity, createdUserAuthEntity.getLogin());
    }

    @Override
    public UserModel getById(long id) {
        User existsUser = userRepositoryJpa.getById(id);

        UserAuth existsAuth = userAuthRepositoryJpa.getById(id);

        if (existsAuth != null) {
            return userMapper.fromWithLogin(existsUser, existsAuth.getLogin());
        }

        return userMapper.from(existsUser);
    }

    @Override
    public UserModel getByLogin(String login) {
        User existsUser = userRepositoryJpa.getByLogin(login);

        return userMapper.from(existsUser);
    }

    @Override
    public String getPasswordHashById(long id) {
        return userAuthRepositoryJpa.getPasswordHashByUserId(id);
    }

    @Override
    public UserModel update(UserModel userModel) {
        return null;
    }

    @Override
    public UserModel updateWithPassword(UserModel userModel, String password) {
        return null;
    }
}
