import java.util.*

plugins {
    id("java")
    alias(libs.plugins.migration.liquibase)
}

group = "anonym.chat.infrastructure"
version = "0.0.1"

fun loadProperties(env: String): Properties {
    val envHashMap = mapOf(
        "dev" to "main",
        "test" to "test"
    );

    val envActive = envHashMap[env];

    return Properties().apply {
        load(file("${project(":src:web").projectDir}/src/${envActive}/resources/liquibase.properties").reader())
    }
}

fun registerLiquibase(env: String) {
    val props = loadProperties(env);

    liquibase {
        if (activities.contains(activities.findByName("main"))) return@liquibase;

        activities.register("main") {
            val url = props["url"]
            val username = props["username"]
            val password = props["password"]
            val changelogFile = props["changelogFile"]
            val classpath = props["classpath"]
            arguments = mapOf(
                "logLevel" to "info",
                "changelogFile" to changelogFile,
                "url" to url,
                "username" to username,
                "password" to password,
                "classpath" to classpath
            )
        }
    }
}

dependencies {
    implementation(platform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))

    implementation(project(":src:core"))

    implementation(libs.spring.context)
    implementation(libs.spring.boot.starter.data.jpa)
    implementation(libs.spring.boot.starter.data.redis)
    runtimeOnly(libs.db.postgresql)
    implementation(libs.db.redis.jedis)
    implementation(libs.db.`object`.minio)
    implementation(libs.support.ffmpeg)
    implementation(libs.support.image.imageio.core)
    implementation(libs.support.image.imageio.metadata)
    implementation(libs.support.image.imageio.jpeg)
    implementation(libs.support.apache.tika)

    liquibaseRuntime(libs.migration.liquibase.core)
    liquibaseRuntime(libs.support.picocli)
    liquibaseRuntime(libs.db.postgresql)

    testImplementation(libs.testng)

    compileOnly(libs.mapstruct.mapper)
    annotationProcessor(libs.mapstruct.annotation.processor)
}

tasks.register("migrateTest") {
    dependsOn("update")

    registerLiquibase("test")
}

tasks.register("migrateApp") {
    dependsOn("update")

    registerLiquibase("dev")
}

tasks.register("dropAllTest") {
    dependsOn("dropAll")

    registerLiquibase("test")
}

tasks.register("dropAllApp") {
    dependsOn("dropAll")

    registerLiquibase("dev")
}