package anonym.chat.web.configs;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.TestConfiguration;

@TestConfiguration
public class CommonConfig {
    @Value("${server.servlet.session.cookie.name}")
    private String sessionCookieName;

    public String getSessionCookieName() {
        return sessionCookieName;
    }
}
