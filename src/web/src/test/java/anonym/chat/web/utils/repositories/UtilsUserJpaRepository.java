package anonym.chat.web.utils.repositories;

import anonym.chat.infrastructure.persistence.models.user.User;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilsUserJpaRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM users ORDER BY id DESC LIMIT 1", nativeQuery = true)
    User getLastUser();

    @Transactional
    @Modifying
    @Query(value = "DELETE FROM users u USING user_auth ua WHERE u.id = ua.user_id AND ua.login = :login", nativeQuery = true)
    void deleteByLogin(@Param("login") String login);
}
