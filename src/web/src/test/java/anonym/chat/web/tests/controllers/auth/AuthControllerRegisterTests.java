package anonym.chat.web.tests.controllers.auth;

import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.constants.errors.ModelsErrorConstants;
import anonym.chat.core.models.user.CreateNewAuthUser;
import anonym.chat.core.models.user.Role;
import anonym.chat.core.models.user.UserAuth;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.infrastructure.persistence.models.user.User;
import anonym.chat.web.TestClass;
import anonym.chat.web.contracts.auth.register.RegisterRequest;
import anonym.chat.web.utils.repositories.UtilsUserJpaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.Assert;
import org.testng.annotations.Test;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestClass
public class AuthControllerRegisterTests extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UtilsUserJpaRepository utilsUserJpaRepository;

    @Autowired
    private UserService userService;

    @Test
    public void emptyRegisterRequest_shouldReturnRegisterResponseAndStatus200() throws Exception {
        // Arrange
        User existsUser = utilsUserJpaRepository.getLastUser();
        long lastUserId = 0;

        if (existsUser != null) {
            lastUserId = existsUser.getId();
        }

        RegisterRequest request = new RegisterRequest(null, null, null);
        String requestString = objectMapper.writeValueAsString(request);

        // Act
        ResultActions result = mvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(requestString));

        // Assert
        result
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.role").value(Role.USER.getValue()))
            .andExpect(jsonPath("$.login").exists());

        String loginIdString = JsonPath.read(result.andReturn().getResponse().getContentAsString(), "$.login");

        long loginId = Long.parseLong(loginIdString);

        Assert.assertTrue(loginId > lastUserId, "Ошибка. Созданный юзер имеет тот же id, что и последний существующий до этого запроса");

        utilsUserJpaRepository.deleteById(loginId);
    }

    @Test
    public void notExistsLoginRegisterRequest_shouldReturnRegisterResponseAndStatus200() throws Exception {
        // Arrange
        String userLogin = "TEST_USER_LOGIN";
        String userPassword = "TEST_USER_PASSWORD";
        RegisterRequest request = new RegisterRequest(Role.USER.getValue(), userLogin, userPassword);
        String requestString = objectMapper.writeValueAsString(request);

        utilsUserJpaRepository.deleteByLogin(userLogin);

        // Act
        ResultActions result = mvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(requestString));

        // Assert
        result
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.role").value(Role.USER.getValue()))
            .andExpect(jsonPath("$.login").value(userLogin));

        utilsUserJpaRepository.deleteByLogin(userLogin);
    }

    @Test
    public void existsLoginRegisterRequest_shouldReturnErrorAndStatus400() throws Exception {
        // Arrange
        String userLogin = "TEST_USER_LOGIN";
        String userPassword = "TEST_USER_PASSWORD";

        RegisterRequest request = new RegisterRequest(Role.USER.getValue(), userLogin, userPassword);
        String requestString = objectMapper.writeValueAsString(request);

        CreateNewAuthUser createNewAuthUser = new CreateNewAuthUser(
                Role.USER,
                new UserAuth(userLogin, userPassword));

        ResultT<UserModel> createdUser = userService.addAuthUser(createNewAuthUser);


        Assert.assertTrue(createdUser.isSuccess());

        // Act
        ResultActions result = mvc.perform(post("/api/auth/register")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(requestString));

        // Assert
        result
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.type").value(ModelsErrorConstants.EXISTS_TYPE))
            .andExpect(jsonPath("$.title").value(ModelsErrorConstants.EXISTS_TITLE))
            .andExpect(jsonPath("$.status").value(ModelsErrorConstants.EXISTS_STATUS))
            .andExpect(jsonPath("$.description").value(ModelsErrorConstants.EXISTS_DESCRIPTION));

        utilsUserJpaRepository.deleteByLogin(userLogin);
    }
}
