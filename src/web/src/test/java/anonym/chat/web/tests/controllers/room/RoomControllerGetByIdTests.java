package anonym.chat.web.tests.controllers.room;

import anonym.chat.core.abstractions.room.RoomService;
import anonym.chat.core.constants.errors.CommonErrorsConstants;
import anonym.chat.core.constants.errors.ModelsErrorConstants;
import anonym.chat.core.models.room.CreateNewRoom;
import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.user.Role;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.TestClass;
import anonym.chat.web.utils.TestWithAuthUser;
import anonym.chat.web.utils.repositories.UtilsRoomJpaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.instancio.Instancio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;

import static org.instancio.Select.field;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestClass
public class RoomControllerGetByIdTests extends TestWithAuthUser {
    static class InnerDataProvider {
        @DataProvider(name = "provider1")
        public static Object[][] provider1() {
            Object[][] array = new Object[2][];

            List<Object> row1 = new ArrayList<>();

            row1.add(-1);

            List<Object> row2 = new ArrayList<>();

            row2.add(0);

            array[0] = row1.toArray();
            array[1] = row2.toArray();

            return array;
        }
    }

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RoomService roomService;

    @Autowired
    private UtilsRoomJpaRepository utilsRoomJpaRepository;


    @Test(dataProvider = "provider1", dataProviderClass = InnerDataProvider.class)
    public void invalidRequest_shouldReturnValidationErrorAndStatus400(long roomId) throws Exception {
        // Arrange
        MockHttpSession userSession = getAuthUserSession(Role.USER).session();

        // Act
        ResultActions result = mvc.perform(get("/api/rooms/{id}", roomId).session(userSession));

        // Assert
        result
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.type").value(CommonErrorsConstants.VALIDATION_TYPE))
            .andExpect(jsonPath("$.title").value(CommonErrorsConstants.VALIDATION_TITLE))
            .andExpect(jsonPath("$.status").value(CommonErrorsConstants.VALIDATION_STATUS))
            .andExpect(jsonPath("$.errors").isNotEmpty());
    }

    @Test
    public void validRequestNotExistsRoomId_shouldReturnNotExistsErrorAndStatus404() throws Exception {
        // Arrange
        MockHttpSession userSession = getAuthUserSession(Role.USER).session();
        long roomId = Long.MAX_VALUE;

        // Act
        ResultActions result = mvc.perform(get("/api/rooms/{id}", roomId).session(userSession));

        // Assert
        result
            .andExpect(status().isNotFound())
            .andExpect(jsonPath("$.type").value(ModelsErrorConstants.NOT_EXISTS_TYPE))
            .andExpect(jsonPath("$.title").value(ModelsErrorConstants.NOT_EXISTS_TITLE))
            .andExpect(jsonPath("$.status").value(ModelsErrorConstants.NOT_EXISTS_STATUS))
            .andExpect(jsonPath("$.description").value(ModelsErrorConstants.NOT_EXISTS_DESCRIPTION));
    }

    @Test
    public void validRequestExistsRoomId_shouldReturnRoomResponseAndStatus200() throws Exception {
        // Arrange
        Tuple tuple = getAuthUserSession(Role.USER);
        UserModel user = tuple.user();
        MockHttpSession userSession = tuple.session();

        CreateNewRoom createNewRoom = Instancio.of(CreateNewRoom.class)
                .set(field(CreateNewRoom::userId), user.getId())
                .create();

        ResultT<RoomModel> createdRoomResult = roomService.add(createNewRoom);

        Assert.assertTrue(createdRoomResult.isSuccess());
        RoomModel room = createdRoomResult.getData();

        // Act
        ResultActions result = mvc.perform(get("/api/rooms/{id}", room.getId()).session(userSession));

        // Assert
        result
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id").value(room.getId()))
            .andExpect(jsonPath("$.userId").value(user.getId()))
            .andExpect(jsonPath("$.name").value(room.getName()))
            .andExpect(jsonPath("$.description").value(room.getDescription()));

        utilsRoomJpaRepository.deleteAll();
    }
}
