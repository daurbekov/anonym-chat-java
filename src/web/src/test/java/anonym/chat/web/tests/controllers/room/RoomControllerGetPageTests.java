package anonym.chat.web.tests.controllers.room;

import anonym.chat.core.abstractions.room.RoomService;
import anonym.chat.core.constants.errors.CommonErrorsConstants;
import anonym.chat.core.models.room.CreateNewRoom;
import anonym.chat.core.models.user.Role;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.web.TestClass;
import anonym.chat.web.utils.TestWithAuthUser;
import anonym.chat.web.utils.repositories.UtilsRoomJpaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.instancio.Instancio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.annotations.*;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.instancio.Select.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@TestClass
public class RoomControllerGetPageTests extends TestWithAuthUser {
    static class InnerDataProvider {
        @DataProvider(name = "provider1")
        public static Object[][] provider1() {
            Object[][] array = new Object[2][];

            List<Object> row1 = new ArrayList<>();

            row1.add(0);

            List<Object> row2 = new ArrayList<>();

            row2.add(-1);

            array[0] = row1.toArray();
            array[1] = row2.toArray();

            return array;
        }
    }

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RoomService roomService;

    @Autowired
    private UtilsRoomJpaRepository utilsRoomJpaRepository;


    private final int TOTAL_IN_PAGE = 10;
    public void initRooms() throws Exception {
        long userId = getAuthUserSession(Role.USER).user().getId();

        List<CreateNewRoom> list = Instancio.ofList(CreateNewRoom.class)
            .size(TOTAL_IN_PAGE)
            .set(field(CreateNewRoom::userId), userId)
            .create();

        list.forEach(i -> {
            roomService.add(i);
        });
    }

    public void deleteRooms() {
        utilsRoomJpaRepository.deleteAll();
    }

    @Test(dataProvider = "provider1", dataProviderClass = InnerDataProvider.class)
    public void invalidRequest_shouldReturnValidationErrorAndStatus400(long page) throws Exception {
        // Arrange
        Tuple tuple = getAuthUserSession(Role.USER);
        MockHttpSession userSession = tuple.session();
        UserModel user = tuple.user();


        // Act
        ResultActions result = mvc.perform(get("/api/rooms/")
                .session(userSession)
                .queryParam("page", Long.toString(page)));

        // Assert
        result
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.type").value(CommonErrorsConstants.VALIDATION_TYPE))
            .andExpect(jsonPath("$.title").value(CommonErrorsConstants.VALIDATION_TITLE))
            .andExpect(jsonPath("$.status").value(CommonErrorsConstants.VALIDATION_STATUS))
            .andExpect(jsonPath("$.errors").isNotEmpty());
    }

    @Test
    public void validRequestEmptyData_shouldReturnSectionResponseWithEmptyItemsAndStatus200() throws Exception {
        // Arrange
        deleteRooms();

        long page = 1;
        Tuple tuple = getAuthUserSession(Role.USER);
        MockHttpSession userSession = tuple.session();
        UserModel user = tuple.user();


        // Act
        ResultActions result = mvc.perform(get("/api/rooms/")
            .session(userSession)
            .queryParam("page", Long.toString(page)));

        // Assert
        result
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.section").value(page))
            .andExpect(jsonPath("$.perSection", greaterThanOrEqualTo(TOTAL_IN_PAGE)))
            .andExpect(jsonPath("$.total", equalTo(0)))
            .andExpect(jsonPath("$.items", hasSize(0)));
    }

    @Test
    public void validRequest_shouldReturnSectionResponseAndStatus200() throws Exception {
        // Arrange
        initRooms();

        long page = 1;
        Tuple tuple = getAuthUserSession(Role.USER);
        MockHttpSession userSession = tuple.session();
        UserModel user = tuple.user();

        // Act
        ResultActions result = mvc.perform(get("/api/rooms/")
            .session(userSession)
            .queryParam("page", Long.toString(page)));

        // Assert
        result
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.section").value(page))
            .andExpect(jsonPath("$.perSection", greaterThanOrEqualTo(TOTAL_IN_PAGE)))
            .andExpect(jsonPath("$.total", equalTo(TOTAL_IN_PAGE)))
            .andExpect(jsonPath("$.items", hasSize(TOTAL_IN_PAGE)))
            .andExpect(jsonPath("$.items[*].id", hasItem(greaterThan(0))))
            .andExpect(jsonPath("$.items[*].userId", hasItem((int)user.getId())))
            .andExpect(jsonPath("$.items[*].name").isNotEmpty())
            .andExpect(jsonPath("$.items[*].description").isNotEmpty());
    }
}
