package anonym.chat.web.tests.controllers.auth;

import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.constants.errors.CommonErrorsConstants;
import anonym.chat.core.constants.validations.CommonValidationMessages;
import anonym.chat.core.models.user.CreateNewAuthUser;
import anonym.chat.core.models.user.Role;
import anonym.chat.core.models.user.UserAuth;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.TestClass;
import anonym.chat.web.contracts.auth.login.LoginRequest;
import anonym.chat.web.utils.repositories.UtilsUserJpaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.test.web.servlet.ResultActions;
import org.testng.Assert;
import org.testng.annotations.Test;

@TestClass
public class AuthControllerLoginTests extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private UserService userService;

    @Autowired
    private UtilsUserJpaRepository utilsUserJpaRepository;

    @Test
    public void emptyLogin_shouldReturnValidationErrorAndStatus400() throws Exception {
        // Arrange
        LoginRequest loginRequest = new LoginRequest("", null);
        String loginRequestJson = objectMapper.writeValueAsString(loginRequest);

        // Act
        ResultActions result = mvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(loginRequestJson));

        // Assert
        result
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.type").value(CommonErrorsConstants.VALIDATION_TYPE))
            .andExpect(jsonPath("$.title").value(CommonErrorsConstants.VALIDATION_TITLE))
            .andExpect(jsonPath("$.status").value(CommonErrorsConstants.VALIDATION_STATUS))
            .andExpect(jsonPath("$.errors.login").exists())
            .andExpect(jsonPath("$.errors.login").value(CommonValidationMessages.FIELD_EMPTY));
    }

    @Test
    public void idLogin_shouldReturnStatus204() throws Exception {
        // Arrange
        ResultT<UserModel> createdUserResult = userService.add();

        if (createdUserResult.isFailure()) {
            Assert.assertTrue(createdUserResult.isSuccess());
        }

        LoginRequest loginRequest = new LoginRequest(
                Long.toString(createdUserResult.getData().getId()), null);

        String loginRequestJson = objectMapper.writeValueAsString(loginRequest);

        // Act
        ResultActions result = mvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(loginRequestJson));

        // Assert
        result.andExpect(status().isNoContent());

        utilsUserJpaRepository.deleteById(createdUserResult.getData().getId());
    }

    @Test
    public void authLogin_shouldReturnStatus204() throws Exception {
        // Arrange
        String login = "SOME_LOGIN";
        String password = "SOME_PASSWORD";

        ResultT<UserModel> existsUserResult = userService.getByLogin(login);

        if (existsUserResult.isFailure()) {
            CreateNewAuthUser createNewAuthUser = new CreateNewAuthUser(
                Role.USER,
                new UserAuth(login, password)
            );

            ResultT<UserModel> createdUserResult = userService.addAuthUser(createNewAuthUser);

            if (createdUserResult.isFailure()) {
                Assert.assertTrue(createdUserResult.isSuccess());
            }

            existsUserResult = createdUserResult;
        }

        LoginRequest loginRequest = new LoginRequest(login, password);
        String loginRequestJson = objectMapper.writeValueAsString(loginRequest);

        // Act
        ResultActions result = mvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(loginRequestJson));

        // Assert
        result.andExpect(status().isNoContent());

        utilsUserJpaRepository.deleteById(existsUserResult.getData().getId());
    }

    @Test
    public void notExistsLogin_shouldReturnErrorAndStatus404() throws Exception {
        // Arrange
        LoginRequest loginRequest = new LoginRequest("DOESN'T EXISTS USER", null);
        String loginRequestJson = objectMapper.writeValueAsString(loginRequest);

        // Act
        ResultActions result = mvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(loginRequestJson));

        // Assert
        result
            .andExpect(status().isUnauthorized())
            .andExpect(jsonPath("$.type").value(CommonErrorsConstants.AUTH_TYPE))
            .andExpect(jsonPath("$.title").value(CommonErrorsConstants.AUTH_TITLE))
            .andExpect(jsonPath("$.status").value(CommonErrorsConstants.AUTH_STATUS))
            .andExpect(jsonPath("$.description").value(CommonErrorsConstants.AUTH_DESCRIPTION_INVALID_PASSWORD_OR_LOGIN));
    }
}
