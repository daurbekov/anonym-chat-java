package anonym.chat.web.utils.repositories;

import anonym.chat.infrastructure.persistence.models.room.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilsRoomJpaRepository extends JpaRepository<Room, Long> {

}
