package anonym.chat.web.tests.controllers.room;

import anonym.chat.core.abstractions.room.RoomService;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.constants.errors.CommonErrorsConstants;
import anonym.chat.core.models.user.Role;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.web.TestClass;
import anonym.chat.web.contracts.room.CreateNewRoomRequest;
import anonym.chat.web.utils.TestWithAuthUser;
import anonym.chat.web.utils.repositories.UtilsRoomJpaRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestClass
public class RoomControllerCreateTests extends TestWithAuthUser {
    static class InnerDataProvider {
        @DataProvider(name = "provider1")
        public static Object[][] provider1() {
            Object[][] array = new Object[2][];

            List<Object> row1 = new ArrayList<>();

            row1.add(null);
            row1.add(null);

            List<Object> row2 = new ArrayList<>();

            row2.add(null);
            row2.add("string");

            array[0] = row1.toArray();
            array[1] = row2.toArray();

            return array;
        }
    }

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private RoomService roomService;


    @Test(dataProvider = "provider1", dataProviderClass = InnerDataProvider.class)
    public void emptyRequest_shouldReturnValidationErrorAndStatus400(String name, String description) throws Exception {
        // Arrange
        CreateNewRoomRequest createNewRoomRequest = new CreateNewRoomRequest(null, null);
        String createNewRoomRequestString = objectMapper.writeValueAsString(createNewRoomRequest);
        MockHttpSession userSession = getAuthUserSession(Role.USER).session();

        // Act
        ResultActions result = mvc.perform(post("/api/rooms/")
            .session(userSession)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(createNewRoomRequestString));

        // Assert
        result
            .andExpect(status().isBadRequest())
            .andExpect(jsonPath("$.type").value(CommonErrorsConstants.VALIDATION_TYPE))
            .andExpect(jsonPath("$.title").value(CommonErrorsConstants.VALIDATION_TITLE))
            .andExpect(jsonPath("$.status").value(CommonErrorsConstants.VALIDATION_STATUS))
            .andExpect(jsonPath("$.errors").exists());
    }

    @Test
    public void validRequest_shouldReturnRoomResponseAndStatus200() throws Exception {
        // Arrange
        String name = "name";
        String description = "description";
        CreateNewRoomRequest createNewRoomRequest = new CreateNewRoomRequest(name, description);
        String createNewRoomRequestString = objectMapper.writeValueAsString(createNewRoomRequest);
        Tuple tuple = getAuthUserSession(Role.USER);
        MockHttpSession userSession = tuple.session();
        UserModel user = tuple.user();

        // Act
        ResultActions result = mvc.perform(post("/api/rooms/")
            .session(userSession)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(createNewRoomRequestString));

        // Assert
        result
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.id", greaterThan(0)))
            .andExpect(jsonPath("$.userId").value(user.getId()))
            .andExpect(jsonPath("$.name").value(name))
            .andExpect(jsonPath("$.description").value(description));

        long createdRoomId = JsonPath.parse(result.andReturn().getResponse().getContentAsString()).read("$.id", Long.class);

        roomService.deleteById(createdRoomId);
    }
}
