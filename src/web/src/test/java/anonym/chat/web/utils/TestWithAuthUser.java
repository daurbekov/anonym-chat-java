package anonym.chat.web.utils;

import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.models.user.CreateNewAuthUser;
import anonym.chat.core.models.user.Role;
import anonym.chat.core.models.user.UserAuth;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.infrastructure.persistence.configs.DatabaseSeederConfig;
import anonym.chat.web.configs.CommonConfig;
import anonym.chat.web.contracts.auth.login.LoginRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public abstract class TestWithAuthUser extends AbstractTestNGSpringContextTests {
    public record Tuple(UserModel user, MockHttpSession session) { }

    @Autowired
    private DatabaseSeederConfig databaseSeederConfig;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private UserService userService;

    @Autowired
    private CommonConfig config;

    protected final String ADMIN_LOGIN = "admin";
    private final String ADMIN_PASSWORD = "admin";

    private final String MODER_LOGIN = "moder";
    private final String MODER_PASSWORD = "moder";

    private final String USER_LOGIN = "user";
    private final String USER_PASSWORD = "user";

    protected Tuple getAuthUserSession(Role role) throws Exception {
        UserModel user = getUserModel(role);

        LoginRequest loginRequest = new LoginRequest(
            user.getLogin() == null ? Long.toString(user.getId()) : user.getLogin(),
            role == Role.USER ? USER_PASSWORD : MODER_PASSWORD
        );

        String loginRequestString = objectMapper.writeValueAsString(loginRequest);

        MvcResult result = mvc.perform(post("/api/auth/login")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(loginRequestString))
                .andReturn();

        MockHttpSession session = (MockHttpSession) result.getRequest().getSession();

        return new Tuple(user, session);
    }

    private UserModel getUserModel(Role role) {
        return switch (role) {
            case ADMIN -> initAdmin();
            case MODERATOR -> initModer();
            case USER -> initUser();
        };
    }

    private UserModel initUsers(String login, String password, Role role) {
        ResultT<UserModel> existsUserResult = null;

        boolean hasId = false;

        try {
            long id = Long.parseLong(login);
            hasId = true;

            existsUserResult = userService.getById(id);
        } catch (Exception ignored) {
            existsUserResult = userService.getByLogin(login);
        }

        if (existsUserResult.isSuccess()) {
            return existsUserResult.getData();
        }

        ResultT<UserModel> createdUserResult = null;

        if (hasId) {
            createdUserResult = userService.add();
        } else {
            createdUserResult = userService.addAuthUser(new CreateNewAuthUser(
                role, new UserAuth(login, password)
            ));
        }

        return createdUserResult.getData();
    }

    private UserModel initModer() {
        return initUsers(MODER_LOGIN, MODER_PASSWORD, Role.MODERATOR);
    }

    private UserModel initUser() {
        return initUsers(USER_LOGIN, USER_PASSWORD, Role.USER);
    }

    private UserModel initAdmin() {
        return initUsers(ADMIN_LOGIN, ADMIN_PASSWORD, Role.ADMIN);
    }
}
