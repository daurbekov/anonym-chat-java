package anonym.chat.web.utils.cookie;

public class CookieExpirationBuilder {
    private int expiration;

    private CookieExpirationBuilder() {
        this.expiration = 0;
    }

    public static CookieExpirationBuilder create() {
        return new CookieExpirationBuilder();
    }

    public CookieExpirationBuilder addSeconds(int seconds) {
        expiration += seconds;

        return this;
    }

    public CookieExpirationBuilder addMinutes(int minutes) {
        expiration += (minutes * 60);

        return this;
    }

    public CookieExpirationBuilder addHours(int hours) {
        expiration += (hours * 60 * 60);

        return this;
    }

    public CookieExpirationBuilder addDays(int days) {
        expiration += (days * 60 * 60 * 24);

        return this;
    }

    public CookieExpirationBuilder addWeeks(int weeks) {
        expiration += (weeks * 60 * 60 * 24 * 7);

        return this;
    }

    public CookieExpirationBuilder addMonths(int months) {
        expiration += (months * 60 * 60 * 24 * 30);

        return this;
    }

    public CookieExpirationBuilder addYears(int years) {
        expiration += (years * 60 * 60 * 24 * 30 * 12);

        return this;
    }

    public int build() {
        return expiration;
    }
}
