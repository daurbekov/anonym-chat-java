package anonym.chat.web.controllers.api.room;

import anonym.chat.core.abstractions.message.room.RoomMessageService;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.message.room.*;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AuthenticatedUserProvider;
import anonym.chat.web.contracts.message.room.CreateNewPrivateMessageRequest;
import anonym.chat.web.contracts.message.room.GetAllDestinationSectionRequest;
import anonym.chat.web.contracts.message.room.GetPrivateAllSectionRequest;
import anonym.chat.web.contracts.message.room.RoomMessageResponse;
import anonym.chat.web.contracts.message.user.UserIdResponse;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.helpers.contracts.RoomMessageHelperContract;
import anonym.chat.web.mappers.CommonMapper;
import anonym.chat.web.mappers.contracts.RoomMessageMapper;
import anonym.chat.web.utils.HttpUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(
    value = "/api/rooms/{roomId}/privates/messages",
    produces = MediaType.APPLICATION_JSON_VALUE
)
public class RoomPrivateMessageController {
    private final RoomMessageService roomMessageService;
    private final RoomMessageMapper roomMessageMapper;
    private final RoomMessageHelperContract roomMessageHelperContract;

    private final AuthenticatedUserProvider authenticatedUserProvider;
    private final CommonMapper commonMapper;

    public RoomPrivateMessageController(
            RoomMessageService roomMessageService,
            RoomMessageMapper roomMessageMapper,
            RoomMessageHelperContract roomMessageHelperContract,
            AuthenticatedUserProvider authenticatedUserProvider,
            CommonMapper commonMapper) {
        this.roomMessageService = roomMessageService;
        this.roomMessageMapper = roomMessageMapper;
        this.roomMessageHelperContract = roomMessageHelperContract;
        this.authenticatedUserProvider = authenticatedUserProvider;
        this.commonMapper = commonMapper;
    }

    @PostMapping
    public ResponseEntity<?> sendPrivateMessage(
            @PathVariable("roomId") long roomId,
            @RequestBody CreateNewPrivateMessageRequest request) {
        UserModel authUser = authenticatedUserProvider.get().get();

        CreateNewPrivateRoomMessage createNewPrivateRoomMessage = new CreateNewPrivateRoomMessage(
            authUser.getId(),
            roomId,
            request.destinationUserId(),
            request.body()
        );

        ResultT<RoomMessageModel> createNewPrivateRoomMessageResult = roomMessageService
                .addPrivateRoomMessage(createNewPrivateRoomMessage);

        if (createNewPrivateRoomMessageResult.isFailure()) {
            return HttpUtils.responseError(createNewPrivateRoomMessageResult.getError(), commonMapper);
        }

        RoomMessageResponse response = roomMessageHelperContract.toRoomMessageResponse(
            createNewPrivateRoomMessageResult.getData(),
            authUser.getId()
        );

        return ResponseEntity.ok(response);
    }

    @GetMapping("/destinations")
    public ResponseEntity<?> readAllDestinationSection(
            @PathVariable("roomId") long roomId,
            GetAllDestinationSectionRequest request) {
        UserModel authUser = authenticatedUserProvider.get().get();

        GetAllPrivateMessageDestinationUserSection get = new GetAllPrivateMessageDestinationUserSection(
            roomId,
            authUser.getId(),
            request.section()
        );

        ResultT<SectionCollection<UserModel>> destinationSectionResult = roomMessageService
                .getAllPrivateMessageDestinationUserSection(get);

        if (destinationSectionResult.isFailure()) {
            return HttpUtils.responseError(destinationSectionResult.getError(), commonMapper);
        }

        SectionResponse<UserIdResponse> destinationUserSection = roomMessageHelperContract
                .toSectionResponseDestinationUserIdFromSectionCollectionUserModel(destinationSectionResult.getData());

        return ResponseEntity.ok(destinationUserSection);
    }

    // TODO: проверка, если пытаюсь указать свой userId как destination, кидать ошибку
    @GetMapping
    public ResponseEntity<?> readPrivateAllSection(
            @PathVariable("roomId") long roomId,
            GetPrivateAllSectionRequest request) {
        UserModel authUser = authenticatedUserProvider.get().get();

        GetAllPrivateMessageByDestination get = new GetAllPrivateMessageByDestination(
                request.section(),
                roomId,
                authUser.getId(),
                request.destinationUserId());

        ResultT<SectionCollection<RoomMessageModel>> section = roomMessageService
                .getAllPrivateMessageByDestinationUserSection(get);

        if (section.isFailure()) {
            return HttpUtils.responseError(section.getError(), commonMapper);
        }

        SectionResponse<RoomMessageResponse> response = roomMessageHelperContract.toSectionResponseRoomMessage(
            section.getData(),
            authUser.getId());

        return ResponseEntity.ok(response);
    }

    @PostMapping("/{messageId}/replies")
    public ResponseEntity<?> replyPrivateMessage(
        @PathVariable("roomId") long roomId,
        @PathVariable("messageId") long messageId,
        @RequestBody CreateNewPrivateMessageRequest request
    ) {
        UserModel authUser = authenticatedUserProvider.get().get();

        ReplyToRoomPrivateMessage reply = new ReplyToRoomPrivateMessage(
            authUser.getId(),
            request.destinationUserId(),
            roomId,
            messageId,
            request.body()
        );

        ResultT<RoomMessageModel> replyResult = roomMessageService.replyToRoomPrivateMessage(reply);

        if (replyResult.isFailure()) {
            return HttpUtils.responseError(replyResult.getError(), commonMapper);
        }

        RoomMessageResponse response = roomMessageHelperContract
                .toRoomMessageResponse(replyResult.getData(), authUser.getId());

        return ResponseEntity.ok(response);
    }
}
