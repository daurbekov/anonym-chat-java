package anonym.chat.web.contracts.avatar;

import org.springframework.web.multipart.MultipartFile;

public record CreateNewAvatarRequest(
    MultipartFile file,
    int offsetX,
    int offsetY,
    int size
) { }
