package anonym.chat.web.contracts.message.room;

public record GetPrivateAllSectionRequest(
    long section,
    long destinationUserId
) { }
