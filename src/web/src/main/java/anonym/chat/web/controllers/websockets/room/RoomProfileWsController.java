package anonym.chat.web.controllers.websockets.room;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class RoomProfileWsController {
    private final SimpMessagingTemplate simpMessagingTemplate;

    public RoomProfileWsController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }


}
