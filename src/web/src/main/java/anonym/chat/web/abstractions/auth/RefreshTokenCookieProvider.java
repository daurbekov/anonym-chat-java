package anonym.chat.web.abstractions.auth;

import anonym.chat.core.models.token.refresh.RefreshSessionModel;
import anonym.chat.core.utils.result.ResultT;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.util.Optional;

public interface RefreshTokenCookieProvider {
    boolean setRefreshToken(RefreshSessionModel refreshSession, HttpServletResponse servletResponse);
    Optional<String> getRefreshToken(HttpServletRequest servletRequest);
    ResultT<Boolean> handleInstallRefreshToken(long userId, HttpServletRequest servletRequest, HttpServletResponse servletResponse);
    ResultT<RefreshSessionModel> handleReinstallRefreshToken(HttpServletRequest servletRequest, HttpServletResponse servletResponse);
}
