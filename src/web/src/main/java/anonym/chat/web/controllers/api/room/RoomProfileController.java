package anonym.chat.web.controllers.api.room;

import anonym.chat.core.abstractions.room.profile.RoomProfileService;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.room.profile.CreateNewRoomProfile;
import anonym.chat.core.models.room.profile.IsInstalledToRoom;
import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.core.models.room.profile.SetRoomProfileToRoom;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AuthenticatedUserProvider;
import anonym.chat.web.contracts.room.profile.*;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.helpers.contracts.RoomProfileHelperContract;
import anonym.chat.web.mappers.CommonMapper;
import anonym.chat.web.mappers.contracts.RoomProfileMapper;
import anonym.chat.web.utils.HttpUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// TODO: возможно подумать над тем, чтобы установку профиля перенести в POST /api/rooms/{id}/profiles/{id}

@RestController
@RequestMapping(
    value = "/api/room-profiles",
    produces = MediaType.APPLICATION_JSON_VALUE)
public class RoomProfileController {
    private final RoomProfileService roomProfileService;

    private final AuthenticatedUserProvider authenticatedUserProvider;
    private final CommonMapper commonMapper;
    private final RoomProfileMapper roomProfileMapper;
    private final RoomProfileHelperContract roomProfileHelperContract;

    public RoomProfileController(
            RoomProfileService roomProfileService,
            AuthenticatedUserProvider authenticatedUserProvider,
            CommonMapper commonMapper,
            RoomProfileMapper roomProfileMapper,
            RoomProfileHelperContract roomProfileHelperContract) {
        this.roomProfileService = roomProfileService;
        this.authenticatedUserProvider = authenticatedUserProvider;
        this.commonMapper = commonMapper;
        this.roomProfileMapper = roomProfileMapper;
        this.roomProfileHelperContract = roomProfileHelperContract;
    }

    /**
     * TODO: Добавить возможность возвращения рандомных аватарок, если не указан аватар айди
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@RequestBody CreateNewRoomProfileRequest request) {
        UserModel authUser = authenticatedUserProvider.get().get();

        CreateNewRoomProfile createNewRoomProfile = roomProfileMapper
                .toCreateNewRoomProfileFromCreateNewRoomProfileRequest(request, authUser.getId());

        ResultT<RoomProfileModel> createRoomProfileResult = roomProfileService.add(createNewRoomProfile);

        if (createRoomProfileResult.isFailure()) {
            return HttpUtils.responseError(createRoomProfileResult.getError(), commonMapper);
        }

        RoomProfileResponse response = roomProfileMapper.toRoomProfileResponseFromRoomProfileModel(createRoomProfileResult.getData());

        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<?> getPage(@RequestParam("page") long page) {
        UserModel authUser = authenticatedUserProvider.get().get();

        ResultT<SectionCollection<RoomProfileModel>> roomProfileResult =
                roomProfileService.getAllByUserIdInPage(authUser.getId(), page);

        if (roomProfileResult.isFailure()) {
            return HttpUtils.responseError(roomProfileResult.getError(), commonMapper);
        }

        SectionResponse<RoomProfileFullResponse> response = roomProfileHelperContract
                .toSectionResponseRoomProfilesFromSection(roomProfileResult.getData());

        return ResponseEntity.ok(response);
    }

    // TODO: возможно, стоит подумать над тем, чтобы можно было просто получить текущий установленный профиль
    @PostMapping(value = "/{id}/installs", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> set(
            @PathVariable("id") long roomProfileId,
            @RequestBody SetRoomProfileRequest request) {
        UserModel authUser = authenticatedUserProvider.get().get();

        SetRoomProfileToRoom setRoomProfileToRoom = roomProfileMapper
                .toSetRoomProfileToRoomFromSetRoomProfileRequest(request, roomProfileId, authUser.getId());

        ResultT<Boolean> resultOfSetting = roomProfileService.setRoomProfileToRoom(setRoomProfileToRoom);

        if (resultOfSetting.isFailure()) {
            return HttpUtils.responseError(resultOfSetting.getError(), commonMapper);
        }

        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/{id}/installs")
    public ResponseEntity<?> isInstalledToRoom(
            @PathVariable("id") long roomProfileId,
            @RequestParam long toRoom) {
        UserModel authUser = authenticatedUserProvider.get().get();

        IsInstalledToRoom isInstalled = new IsInstalledToRoom(
            toRoom,
            roomProfileId,
            authUser.getId()
        );

        ResultT<Boolean> isInstalledResult = roomProfileService
                .isInstalledToRoom(isInstalled);

        if (isInstalledResult.isFailure()) {
            return HttpUtils.responseError(isInstalledResult.getError(), commonMapper);
        }

        IsInstalledResponse response = new IsInstalledResponse(isInstalledResult.getData());

        return ResponseEntity.ok(response);
    }
}
