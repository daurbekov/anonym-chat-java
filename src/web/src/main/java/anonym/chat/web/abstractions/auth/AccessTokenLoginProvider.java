package anonym.chat.web.abstractions.auth;

import anonym.chat.core.utils.result.ResultT;
import jakarta.servlet.http.HttpServletRequest;

public interface AccessTokenLoginProvider {
    ResultT<Boolean> login(String accessToken, HttpServletRequest servletRequest);
    ResultT<Boolean> login(String accessToken);
}
