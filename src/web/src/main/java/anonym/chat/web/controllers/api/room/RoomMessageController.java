package anonym.chat.web.controllers.api.room;

import anonym.chat.core.abstractions.message.room.RoomMessageService;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.message.room.*;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AuthenticatedUserProvider;
import anonym.chat.web.contracts.message.CreateNewMessageRequest;
import anonym.chat.web.contracts.message.CreateNewMessageResponse;
import anonym.chat.web.contracts.message.room.RoomMessageResponse;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.helpers.contracts.RoomMessageHelperContract;
import anonym.chat.web.mappers.CommonMapper;
import anonym.chat.web.mappers.contracts.RoomMessageMapper;
import anonym.chat.web.utils.HttpUtils;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

// TODO: при отправке сообщения какому-то юзеру, нужно проверять, существовал ли он в комнате вообще, если нет, то выбрасывать 404
@RestController
@RequestMapping(
    value = "/api/rooms/{roomId}/messages",
    produces = MediaType.APPLICATION_JSON_VALUE
)
public class RoomMessageController {
    private final RoomMessageService roomMessageService;
    private final RoomMessageMapper roomMessageMapper;
    private final RoomMessageHelperContract roomMessageHelperContract;

    private final AuthenticatedUserProvider authenticatedUserProvider;
    private final CommonMapper commonMapper;

    public RoomMessageController(
            RoomMessageService roomMessageService,
            RoomMessageMapper roomMessageMapper,
            RoomMessageHelperContract roomMessageHelperContract,
            AuthenticatedUserProvider authenticatedUserProvider,
            CommonMapper commonMapper) {
        this.roomMessageService = roomMessageService;
        this.roomMessageMapper = roomMessageMapper;
        this.roomMessageHelperContract = roomMessageHelperContract;
        this.authenticatedUserProvider = authenticatedUserProvider;
        this.commonMapper = commonMapper;
    }


    @PostMapping
    public ResponseEntity<?> sendMessage(
            @PathVariable("roomId") long roomId,
            @RequestBody CreateNewMessageRequest request) {
        UserModel authUser = authenticatedUserProvider.get().get();

        CreateNewRoomMessage createNewRoomMessage = roomMessageMapper
                .toCreateNewRoomMessageFromCreateNewRoomMessageRequest(request, authUser.getId(), roomId);

        ResultT<RoomMessageModel> createdRoomMessageResult = roomMessageService
                .addPublicRoomMessage(createNewRoomMessage);

        if (createdRoomMessageResult.isFailure()) {
            return HttpUtils.responseError(createdRoomMessageResult.getError(), commonMapper);
        }

        CreateNewMessageResponse response = roomMessageHelperContract
                .toCreateNewMessageResponse(createdRoomMessageResult.getData());

        return ResponseEntity.ok(response);
    }

    // TODO: поправить пагинцию, возвращать максимальное кол-во страниц/секций, возможно возвращать также и с sectionResponse это
    @GetMapping
    public ResponseEntity<?> readAllSection(@PathVariable("roomId") long roomId, @RequestParam long section) {
        UserModel authUser = authenticatedUserProvider.get().get();

        GetAllInRoomSection getAllInRoomSection = new GetAllInRoomSection(roomId, section, authUser.getId());

        ResultT<SectionCollection<RoomMessageModel>> sectionResult = roomMessageService
                .getAllInRoomSection(getAllInRoomSection);

        if (sectionResult.isFailure()) {
            return HttpUtils.responseError(sectionResult.getError(), commonMapper);
        }

        SectionResponse<RoomMessageResponse> sectionResponse = roomMessageHelperContract
                .toSectionResponseRoomMessage(sectionResult.getData(), authUser.getId());

        return ResponseEntity.ok(sectionResponse);
    }

    @GetMapping("/{messageId}")
    public ResponseEntity<?> readMessage(
            @PathVariable("roomId") long roomId,
            @PathVariable("messageId") long messageId) {
        UserModel authUser = authenticatedUserProvider.get().get();

        GetInRoomById getInRoomById = new GetInRoomById(roomId, authUser.getId(), messageId);

        ResultT<RoomMessageModel> messageResult = roomMessageService.getInRoomById(getInRoomById);

        if (messageResult.isFailure()) {
            return HttpUtils.responseError(messageResult.getError(), commonMapper);
        }

        RoomMessageResponse response = roomMessageHelperContract
                .toRoomMessageResponse(messageResult.getData(), authUser.getId());

        return ResponseEntity.ok(response);
    }

    @PostMapping("/{messageId}/replies")
    public ResponseEntity<?> replyMessage(
            @PathVariable("roomId") long roomId,
            @PathVariable("messageId") long messageId,
            @RequestBody CreateNewMessageRequest request) {
        UserModel authUser = authenticatedUserProvider.get().get();

        ReplyRoomMessage replyRoomMessage = new ReplyRoomMessage(
            authUser.getId(),
            roomId,
            messageId,
            request.body()
        );

        ResultT<RoomMessageModel> repliedRoomMessageResult = roomMessageService.replyToRoomMessage(replyRoomMessage);

        if (repliedRoomMessageResult.isFailure()) {
            return HttpUtils.responseError(repliedRoomMessageResult.getError(), commonMapper);
        }

        RoomMessageResponse response = roomMessageHelperContract
                .toRoomMessageResponse(repliedRoomMessageResult.getData(), authUser.getId());

        return ResponseEntity.ok(response);
    }
}
