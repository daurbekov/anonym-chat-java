package anonym.chat.web.contracts.file;

public record FileResponse(
    long id,
    long size,
    int fileType,
    String url
) { }
