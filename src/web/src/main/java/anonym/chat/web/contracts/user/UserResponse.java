package anonym.chat.web.contracts.user;

import java.time.LocalDateTime;

public record UserResponse(
    long id,
    int role,
    String login,
    LocalDateTime createdAt
) { }
