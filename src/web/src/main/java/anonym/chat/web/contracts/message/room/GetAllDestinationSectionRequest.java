package anonym.chat.web.contracts.message.room;

public record GetAllDestinationSectionRequest(
    long section
) { }
