package anonym.chat.web.contracts.room.profile;

public record RoomProfileResponse(
    long id,
    long avatarId,
    String nickname
) { }
