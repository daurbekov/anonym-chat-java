package anonym.chat.web.contracts.message.user;

import anonym.chat.web.contracts.message.MessagePreviewResponse;

import java.time.LocalDateTime;

public record UserMessageResponse(
    long id,
    long userId,
    MessagePreviewResponse reply,
    String body,
    LocalDateTime createdAt
) { }
