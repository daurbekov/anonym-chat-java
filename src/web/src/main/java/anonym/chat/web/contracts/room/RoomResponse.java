package anonym.chat.web.contracts.room;

public record RoomResponse(
    long id,
    long userId,
    long countConnectedUsers,
    String name,
    String description
) { }
