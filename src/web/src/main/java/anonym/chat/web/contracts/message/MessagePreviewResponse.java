package anonym.chat.web.contracts.message;

public record MessagePreviewResponse(
    long userId,
    long messageId,
    String shortBody
) { }
