package anonym.chat.web.security;

import anonym.chat.core.abstractions.token.refresh.RefreshSessionService;
import anonym.chat.core.models.token.refresh.CreateNewFromExistingToken;
import anonym.chat.core.models.token.refresh.RefreshSessionModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.web.abstractions.auth.RefreshTokenCookieProvider;
import anonym.chat.web.constants.errors.CommonErrorsConstants;
import anonym.chat.web.constants.errors.RefreshTokenErrorsConstants;
import anonym.chat.web.utils.cookie.CookieExpirationBuilder;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Service
public class RefreshTokenCookieProviderImpl implements RefreshTokenCookieProvider {
    private final String REFRESH_TOKEN_COOKIE_NAME = "refresh_token";

    private final RefreshSessionService refreshSessionService;

    public RefreshTokenCookieProviderImpl(
            RefreshSessionService refreshSessionService) {
        this.refreshSessionService = refreshSessionService;
    }

    @Override
    public boolean setRefreshToken(
            RefreshSessionModel refreshSession,
            HttpServletResponse servletResponse) {
        Cookie refreshTokenCookie = new Cookie(REFRESH_TOKEN_COOKIE_NAME, refreshSession.getRefreshToken());

        int maxAge = CookieExpirationBuilder.create()
                .addMonths(1)
                .build();

        refreshTokenCookie.setMaxAge(maxAge);
        refreshTokenCookie.setHttpOnly(true);
        refreshTokenCookie.setPath("/api/auth");

        servletResponse.addCookie(refreshTokenCookie);

        return true;
    }

    @Override
    public Optional<String> getRefreshToken(HttpServletRequest servletRequest) {
        if (servletRequest.getCookies() == null) {
            return Optional.empty();
        }

        Optional<Cookie> existsRefreshTokenCookie = Arrays.stream(servletRequest.getCookies())
                .filter(c -> c.getName().equals(REFRESH_TOKEN_COOKIE_NAME))
                .findFirst();

        return existsRefreshTokenCookie.flatMap(e -> Optional.ofNullable(e.getValue()));
    }

    @Override
    public ResultT<Boolean> handleInstallRefreshToken(
            long userId,
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {
        Optional<String> existsRefreshTokenInCookie = getRefreshToken(servletRequest);

        existsRefreshTokenInCookie.ifPresent(refreshSessionService::deleteByToken);

        ResultT<RefreshSessionModel> createdNewRefreshSessionResult = refreshSessionService
                .createNew(userId);

        if (createdNewRefreshSessionResult.isFailure()) {
            return ResultUtils.fromTSingleError(createdNewRefreshSessionResult.getError());
        }

        return ResultUtils.fromTData(
            setRefreshToken(createdNewRefreshSessionResult.getData(), servletResponse)
        );
    }

    @Override
    public ResultT<RefreshSessionModel> handleReinstallRefreshToken(HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
        Optional<String> existsRefreshTokenInCookie = getRefreshToken(servletRequest);

        if (existsRefreshTokenInCookie.isEmpty()) {
            return ResultUtils.fromTSingleError(
                RefreshTokenErrorsConstants.NOT_EXISTS_COOKIE_TYPE,
                RefreshTokenErrorsConstants.NOT_EXISTS_COOKIE_TITLE,
                RefreshTokenErrorsConstants.NOT_EXISTS_COOKIE_STATUS,
                RefreshTokenErrorsConstants.NOT_EXISTS_COOKIE_DESCRIPTION
            );
        }

        CreateNewFromExistingToken createNewFromExistingToken = new CreateNewFromExistingToken(
            existsRefreshTokenInCookie.get()
        );

        ResultT<RefreshSessionModel> createRefreshSessionResult = refreshSessionService
                .createNewFromExistingToken(createNewFromExistingToken);

        if (createRefreshSessionResult.isFailure()) {
            return ResultUtils.fromTSingleError(createRefreshSessionResult.getError());
        }

        boolean cookieIsInstalled = setRefreshToken(createRefreshSessionResult.getData(), servletResponse);

        if (!cookieIsInstalled) {
            return ResultUtils.fromTSingleError(
                CommonErrorsConstants.CANNOT_INSTALL_COOKIE_TYPE,
                CommonErrorsConstants.CANNOT_INSTALL_COOKIE_TITLE,
                CommonErrorsConstants.CANNOT_INSTALL_COOKIE_STATUS,
                CommonErrorsConstants.CANNOT_INSTALL_COOKIE_DESCRIPTION
            );
        }

        return createRefreshSessionResult;
    }
}
