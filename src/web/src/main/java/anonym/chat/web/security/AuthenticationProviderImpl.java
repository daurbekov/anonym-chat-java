package anonym.chat.web.security;

import anonym.chat.core.abstractions.Validator;
import anonym.chat.core.constants.errors.CommonErrorsConstants;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.core.utils.validator.ValidationResult;
import anonym.chat.core.utils.validator.ValidatorUtils;
import anonym.chat.web.abstractions.auth.AuthenticationProvider;
import anonym.chat.web.validations.auth.LoginValidator;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

@Service
public class AuthenticationProviderImpl implements AuthenticationProvider {
    private final AuthenticationManager authenticationManager;
    private final Validator validator;

    public AuthenticationProviderImpl(
            AuthenticationManager authenticationManager,
            Validator validator) {
        this.authenticationManager = authenticationManager;
        this.validator = validator;
    }

    @Override
    public ResultT<Boolean> authenticate(String principal) {
        return authenticate(principal, principal);
    }

    @Override
    public ResultT<Boolean> authenticate(String login, String password) {
        LoginValidator authValidation = new LoginValidator(login, password);
        ValidationResult validationResult = validator.validate(authValidation);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        UsernamePasswordAuthenticationToken loginPayload = new UsernamePasswordAuthenticationToken(login, password);

        try {
            authenticationManager.authenticate(loginPayload);
        } catch (AuthenticationException e) {
            return ResultUtils.fromTSingleError(
                CommonErrorsConstants.AUTH_TYPE,
                CommonErrorsConstants.AUTH_TITLE,
                CommonErrorsConstants.AUTH_STATUS,
                CommonErrorsConstants.AUTH_DESCRIPTION_INVALID_PASSWORD_OR_LOGIN
            );
        }

        return ResultUtils.fromTData(true);
    }
}
