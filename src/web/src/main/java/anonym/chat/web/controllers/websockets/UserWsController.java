package anonym.chat.web.controllers.websockets;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

@Controller
public class UserWsController {
    private final SimpMessagingTemplate simpMessagingTemplate;

    public UserWsController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }
}
