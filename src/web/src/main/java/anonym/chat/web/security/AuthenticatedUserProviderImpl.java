package anonym.chat.web.security;

import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AuthenticatedUserProvider;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthenticatedUserProviderImpl implements AuthenticatedUserProvider {
    private final UserService userService;

    public AuthenticatedUserProviderImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    public Optional<UserModel> get() {
        Object principalObject = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principalObject == null) {
            return Optional.empty();
        }

        ResultT<UserModel> targetUserResult = null;

        UserDetails userDetails = (UserDetails)principalObject;

        String login = userDetails.getUsername();

        try {
            long id = Long.parseLong(login);

            targetUserResult = userService.getById(id);
        } catch (Exception ignored) {
            targetUserResult = userService.getByLogin(login);
        }

        return Optional.ofNullable(targetUserResult.getData());
    }
}
