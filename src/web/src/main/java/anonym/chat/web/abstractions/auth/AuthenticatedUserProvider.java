package anonym.chat.web.abstractions.auth;

import anonym.chat.core.models.user.UserModel;

import java.util.Optional;

public interface AuthenticatedUserProvider {
    Optional<UserModel> get();
}
