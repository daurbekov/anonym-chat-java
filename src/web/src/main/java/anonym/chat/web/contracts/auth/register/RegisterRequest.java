package anonym.chat.web.contracts.auth.register;


import org.springframework.web.bind.annotation.RequestParam;

public record RegisterRequest(
    @RequestParam(required = false) Integer role,
    @RequestParam(required = false) String login,
    @RequestParam(required = false) String password
) { }
