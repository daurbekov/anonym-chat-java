package anonym.chat.web.controllers.api;

import anonym.chat.core.abstractions.avatar.AvatarService;
import anonym.chat.core.abstractions.file.FileService;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.core.models.avatar.CreateNewAvatar;
import anonym.chat.core.models.avatar.GetAllOfUserSection;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AuthenticatedUserProvider;
import anonym.chat.web.configs.FileCloudConfig;
import anonym.chat.web.contracts.avatar.AvatarResponse;
import anonym.chat.web.contracts.avatar.CreateNewAvatarRequest;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.helpers.contracts.AvatarHelperContract;
import anonym.chat.web.mappers.CommonMapper;
import anonym.chat.web.mappers.contracts.AvatarMapper;
import anonym.chat.web.utils.HttpUtils;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

/**
 * TODO: добавить возможность получения списка своих аватаров
 * TODO: добавить возможность работы с дефолтными аватарками
 */

@RestController
@RequestMapping("/api/avatars")
public class AvatarController {
    private final AvatarService avatarService;
    private final AvatarHelperContract avatarHelperContract;

    private final FileCloudConfig fileCloudConfig;
    private final FileService fileService;
    private final AuthenticatedUserProvider authenticatedUserProvider;
    private final AvatarMapper avatarMapper;
    private final CommonMapper commonMapper;

    public AvatarController(
            AvatarService avatarService,
            AvatarHelperContract avatarHelperContract,
            FileCloudConfig fileCloudConfig,
            FileService fileService,
            AuthenticatedUserProvider authenticatedUserProvider,
            AvatarMapper avatarMapper,
            CommonMapper commonMapper) {
        this.avatarService = avatarService;
        this.avatarHelperContract = avatarHelperContract;
        this.fileCloudConfig = fileCloudConfig;
        this.fileService = fileService;
        this.authenticatedUserProvider = authenticatedUserProvider;
        this.avatarMapper = avatarMapper;
        this.commonMapper = commonMapper;
    }


    @PostMapping(
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> add(@RequestBody CreateNewAvatarRequest request) {
        Optional<UserModel> authUser = authenticatedUserProvider.get();

        if (authUser.isEmpty()) {
            return ResponseEntity.internalServerError().build();
        }

        UserModel user = authUser.get();

        CreateNewAvatar createNewAvatar = null;

        try {
            createNewAvatar = avatarMapper
                .toCreateNewAvatarFromCreateNewAvatarRequest(
                        request,
                        user.getId(),
                        request.file().getInputStream());
        } catch (Exception e) {
            return ResponseEntity.internalServerError().build();
        }

        ResultT<AvatarModel> addResult = avatarService.add(createNewAvatar);

        if (addResult.isFailure()) {
            return HttpUtils.responseError(addResult.getError(), commonMapper);
        }

        AvatarResponse response = avatarHelperContract.toAvatarResponseFromAvatarModel(addResult.getData());

        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") long avatarId) {
        UserModel authUser = authenticatedUserProvider.get().get();

        ResultT<AvatarModel> existsAvatarResult = avatarService.getOfUserById(avatarId, authUser.getId());

        if (existsAvatarResult.isFailure()) {
            return HttpUtils.responseError(existsAvatarResult.getError(), commonMapper);
        }

        AvatarResponse response = avatarHelperContract.toAvatarResponseFromAvatarModel(existsAvatarResult.getData());

        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<?> getAll(@RequestParam long section) {
        UserModel authUser = authenticatedUserProvider.get().get();

        GetAllOfUserSection get = new GetAllOfUserSection(authUser.getId(), section);

        ResultT<SectionCollection<AvatarModel>> sectionResult = avatarService.getAllOfUserSection(get);

        if (sectionResult.isFailure()) {
            return HttpUtils.responseError(sectionResult.getError(), commonMapper);
        }

        SectionResponse<AvatarResponse> response = avatarHelperContract
                .toSectionResponseAvatarFromSectionCollectionAvatar(sectionResult.getData());

        return ResponseEntity.ok(response);
    }
}
