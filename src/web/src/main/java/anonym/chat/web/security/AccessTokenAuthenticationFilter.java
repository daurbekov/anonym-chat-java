package anonym.chat.web.security;

import anonym.chat.core.abstractions.token.AccessTokenService;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AccessTokenLoginProvider;
import anonym.chat.web.abstractions.auth.LoginProvider;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
public class AccessTokenAuthenticationFilter extends OncePerRequestFilter {
    private static final String BEARER_PREFIX = "Bearer ";
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    private final AccessTokenLoginProvider accessTokenLoginProvider;


    public AccessTokenAuthenticationFilter(AccessTokenLoginProvider accessTokenLoginProvider) {
        this.accessTokenLoginProvider = accessTokenLoginProvider;
    }

    @Override
    protected void doFilterInternal(
            @NonNull HttpServletRequest request,
            @NonNull HttpServletResponse response,
            @NonNull FilterChain filterChain) throws ServletException, IOException {

        String authHeader = request.getHeader(AUTHORIZATION_HEADER_NAME);
        if (StringUtils.isEmpty(authHeader) || !StringUtils.startsWith(authHeader, BEARER_PREFIX)) {
            filterChain.doFilter(request, response);
            return;
        }

        String accessToken = authHeader.substring(BEARER_PREFIX.length());

        ResultT<Boolean> accessTokenAuthenticationResult = accessTokenLoginProvider
                .login(accessToken, request);

        filterChain.doFilter(request, response);
    }
}
