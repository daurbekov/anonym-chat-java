package anonym.chat.web.security;

import anonym.chat.core.abstractions.token.AccessTokenService;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.web.abstractions.auth.AccessTokenLoginProvider;
import anonym.chat.web.abstractions.auth.LoginProvider;
import jakarta.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public class AccessTokenLoginProviderImpl implements AccessTokenLoginProvider {
    private final AccessTokenService accessTokenService;
    private final LoginProvider loginProvider;

    public AccessTokenLoginProviderImpl(
            AccessTokenService accessTokenService,
            LoginProvider loginProvider) {
        this.accessTokenService = accessTokenService;
        this.loginProvider = loginProvider;
    }

    @Override
    public ResultT<Boolean> login(String accessToken, HttpServletRequest servletRequest) {
        ResultT<String> userPrincipalResult = getUserPrincipal(accessToken);

        if (userPrincipalResult.isFailure()) {
            return ResultUtils.fromTSingleError(userPrincipalResult.getError());
        }

        return loginProvider.login(userPrincipalResult.getData(), servletRequest);
    }

    @Override
    public ResultT<Boolean> login(String accessToken) {
        ResultT<String> userPrincipalResult = getUserPrincipal(accessToken);

        if (userPrincipalResult.isFailure()) {
            return ResultUtils.fromTSingleError(userPrincipalResult.getError());
        }

        return loginProvider.login(userPrincipalResult.getData());
    }

    private ResultT<String> getUserPrincipal(String accessToken) {
        ResultT<Boolean> isValidResult = accessTokenService.checkTokenValid(accessToken);

        if (isValidResult.isFailure()) {
            return ResultUtils.fromTSingleError(isValidResult.getError());
        }

        ResultT<UserModel> existsUserResult = accessTokenService.getUserByToken(accessToken);

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        UserModel existsUser = existsUserResult.getData();

        String userPrincipal = StringUtils.isEmpty(existsUser.getLogin())
                ? String.valueOf(existsUser.getId())
                : existsUser.getLogin();

        return ResultUtils.fromTData(userPrincipal);
    }
}
