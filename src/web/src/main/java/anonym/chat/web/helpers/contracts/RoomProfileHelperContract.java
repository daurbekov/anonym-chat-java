package anonym.chat.web.helpers.contracts;

import anonym.chat.core.abstractions.avatar.AvatarService;
import anonym.chat.core.abstractions.file.FileService;
import anonym.chat.core.abstractions.room.profile.RoomProfileService;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.core.models.file.FileModel;
import anonym.chat.core.models.room.profile.RoomProfileModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.web.abstractions.auth.AuthenticatedUserProvider;
import anonym.chat.web.configs.FileCloudConfig;
import anonym.chat.web.contracts.avatar.AvatarResponse;
import anonym.chat.web.contracts.room.profile.RoomProfileFullResponse;
import anonym.chat.web.contracts.room.profile.RoomProfileWithUserFullResponse;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.mappers.contracts.AvatarMapper;
import anonym.chat.web.mappers.contracts.RoomProfileMapper;
import anonym.chat.web.mappers.contracts.UserMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RoomProfileHelperContract {
    private final RoomProfileService roomProfileService;
    private final AvatarService avatarService;
    private final FileService fileService;

    private final FileCloudConfig fileCloudConfig;

    private final RoomProfileMapper roomProfileMapper;
    private final AvatarMapper avatarMapper;
    private final UserMapper userMapper;
    private final AuthenticatedUserProvider authenticatedUserProvider;
    private final AvatarHelperContract avatarHelperContract;

    public RoomProfileHelperContract(
        RoomProfileService roomProfileService,
        AvatarService avatarService,
        FileService fileService,
        FileCloudConfig fileCloudConfig,
        RoomProfileMapper roomProfileMapper,
        AvatarMapper avatarMapper,
        UserMapper userMapper,
        AuthenticatedUserProvider authenticatedUserProvider,
        AvatarHelperContract avatarHelperContract
    ) {
        this.roomProfileService = roomProfileService;
        this.avatarService = avatarService;
        this.fileService = fileService;
        this.fileCloudConfig = fileCloudConfig;
        this.roomProfileMapper = roomProfileMapper;
        this.avatarMapper = avatarMapper;
        this.userMapper = userMapper;
        this.authenticatedUserProvider = authenticatedUserProvider;
        this.avatarHelperContract = avatarHelperContract;
    }

    public RoomProfileFullResponse toRoomProfileFullResponseFromRoomProfile(RoomProfileModel roomProfileModel) {
        UserModel authUser = authenticatedUserProvider.get().get();
        AvatarModel avatar = avatarService.getOfUserById(roomProfileModel.getAvatarId(), authUser.getId()).getData();

        AvatarResponse avatarResponse = avatarHelperContract.toAvatarResponseFromAvatarModel(avatar);

        return roomProfileMapper.toRoomProfileFullResponseFrom(roomProfileModel, avatarResponse);
    }

    public SectionResponse<RoomProfileFullResponse> toSectionResponseRoomProfilesFromSection(SectionCollection<RoomProfileModel> section) {
        List<RoomProfileFullResponse> convertedList = section.items().stream()
                .map(this::toRoomProfileFullResponseFromRoomProfile)
                .toList();

        return new SectionResponse<>(
            section.section(),
            section.lastSection(),
            section.perSection(),
            section.total(),
            convertedList
        );
    }

    public RoomProfileWithUserFullResponse generateRoomProfileWithUserFullResponse(
            UserModel userModel, long roomId) {
        RoomProfileModel roomProfile = roomProfileService.getByRoomIdAndUserId(roomId, userModel.getId()).getData();
        AvatarModel avatar = avatarService.getOfUserById(roomProfile.getAvatarId(), userModel.getId()).getData();
        FileModel avatarFile = fileService.getById(avatar.getFileId()).getData();

        String imageUrl = fileCloudConfig.getBaseUrlOfFileStorage() + "/" + avatarFile.getUrl();

        return roomProfileMapper.toRoomProfileWithUserFullResponseFrom(
            roomProfile,
            avatar,
            avatarFile,
            userModel,
            imageUrl
        );
    }

    public SectionResponse<RoomProfileWithUserFullResponse> generateSectionRoomProfileWithUserFullResponse(
            SectionCollection<UserModel> sectionUsers, long roomId) {
        List<RoomProfileWithUserFullResponse> list = sectionUsers.items().stream()
                .map(u -> generateRoomProfileWithUserFullResponse(u, roomId))
                .toList();

        return new SectionResponse<>(
            sectionUsers.section(),
            sectionUsers.lastSection(),
            sectionUsers.perSection(),
            sectionUsers.total(),
            list
        );
    }
}
