package anonym.chat.web.helpers.contracts;

import anonym.chat.core.abstractions.message.room.RoomMessageService;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.message.room.GetInRoomById;
import anonym.chat.core.models.message.room.RoomMessageModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.web.contracts.message.CreateNewMessageResponse;
import anonym.chat.web.contracts.message.room.RoomMessageResponse;
import anonym.chat.web.contracts.message.user.UserIdResponse;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.mappers.contracts.RoomMessageMapper;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RoomMessageHelperContract {
    private final RoomMessageMapper roomMessageMapper;

    private final RoomMessageService roomMessageService;

    public RoomMessageHelperContract(
            RoomMessageMapper roomMessageMapper,
            RoomMessageService roomMessageService) {
        this.roomMessageMapper = roomMessageMapper;
        this.roomMessageService = roomMessageService;
    }


    public CreateNewMessageResponse toCreateNewMessageResponse(RoomMessageModel target) {
        return new CreateNewMessageResponse(target.getId(), target.getCreatedAt());
    }

    public RoomMessageResponse toRoomMessageResponse(RoomMessageModel target, long userIdForPrivates) {
        RoomMessageModel replyModel = target.getReply() == null
                ? null
                : roomMessageService.getInRoomById(new GetInRoomById(target.getRoomId(), userIdForPrivates, target.getReply()))
                .getData();


        return roomMessageMapper.toRoomMessageResponseFrom(target, replyModel);
    }

    public SectionResponse<RoomMessageResponse> toSectionResponseRoomMessage(SectionCollection<RoomMessageModel> target, long userIdForPrivates) {
        List<RoomMessageModel> list = target.items();

        List<RoomMessageResponse> convertedItems = list.stream().map(rm -> toRoomMessageResponse(rm, userIdForPrivates)).toList();

        return new SectionResponse<>(
            target.section(),
            target.lastSection(),
            target.perSection(),
            target.total(),
            convertedItems
        );
    }

    public SectionResponse<UserIdResponse> toSectionResponseDestinationUserIdFromSectionCollectionUserModel(SectionCollection<UserModel> target) {
        List<UserIdResponse> convertedList = target.items().stream()
                .map(u -> new UserIdResponse(u.getId()))
                .toList();

        return new SectionResponse<>(
            target.section(),
            target.lastSection(),
            target.perSection(),
            target.total(),
            convertedList
        );
    }
}
