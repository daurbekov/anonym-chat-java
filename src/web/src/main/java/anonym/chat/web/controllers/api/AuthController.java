package anonym.chat.web.controllers.api;

import anonym.chat.core.abstractions.token.AccessTokenService;
import anonym.chat.core.abstractions.token.refresh.RefreshSessionService;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.models.token.refresh.RefreshSessionModel;
import anonym.chat.core.models.user.CreateNewAuthUser;
import anonym.chat.core.models.user.Role;
import anonym.chat.core.models.user.UserAuth;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AuthenticatedUserProvider;
import anonym.chat.web.abstractions.auth.AuthenticationProvider;
import anonym.chat.web.abstractions.auth.RefreshTokenCookieProvider;
import anonym.chat.web.contracts.auth.login.LoginRequest;
import anonym.chat.web.contracts.auth.register.RegisterRequest;
import anonym.chat.web.contracts.auth.register.RegisterResponse;
import anonym.chat.web.contracts.auth.token.TokensResponse;
import anonym.chat.web.contracts.error.Error;
import anonym.chat.web.contracts.error.ListError;
import anonym.chat.web.mappers.CommonMapper;
import anonym.chat.web.mappers.contracts.UserMapper;
import anonym.chat.web.utils.HttpUtils;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(
    value = "/api/auth",
    produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {
    private final AuthenticationProvider authenticationProvider;
    private final UserService userService;
    private final CommonMapper commonMapper;
    private final UserMapper userMapper;
    private final AccessTokenService accessTokenService;
    RefreshTokenCookieProvider refreshTokenCookieProvider;

    public AuthController(
            AuthenticationProvider authenticationProvider,
            UserService userService,
            CommonMapper commonMapper,
            UserMapper userMapper,
            AccessTokenService accessTokenService,
            RefreshTokenCookieProvider refreshTokenCookieProvider) {
        this.authenticationProvider = authenticationProvider;
        this.commonMapper = commonMapper;
        this.userService = userService;
        this.userMapper = userMapper;
        this.accessTokenService = accessTokenService;
        this.refreshTokenCookieProvider = refreshTokenCookieProvider;
    }

    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = RegisterResponse.class)))
    @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(oneOf = { ListError.class, Error.class })))
    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody RegisterRequest registerRequest) {
        ResultT<UserModel> createUserResult = null;

        if (registerRequest.role() == null ||
                registerRequest.login() == null ||
                registerRequest.password() == null) {
            createUserResult = userService.add();
        } else {
            var createNewAuthUser = new CreateNewAuthUser(
                    Role.fromInt(registerRequest.role()),
                    new UserAuth(registerRequest.login(), registerRequest.password()));

            createUserResult = userService.addAuthUser(createNewAuthUser);
        }

        if (createUserResult.isFailure()) {
            return HttpUtils.responseError(createUserResult.getError(), commonMapper);
        }

        RegisterResponse response = userMapper.toRegisterResponseFromUserModel(createUserResult.getData());

        return ResponseEntity.ok(response);
    }

    @PostMapping("/logout")
    public ResponseEntity<?> fakeLogout() {
        throw new IllegalCallerException("Этот метод не должен вызываться. Имплементируется через spring security");
    }

    @ApiResponse(responseCode = "204")
    @ApiResponse(responseCode = "401", content = @Content(schema = @Schema(implementation = Error.class)))
    @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(implementation = ListError.class)))
    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody LoginRequest loginRequest, HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
        ResultT<Boolean> loginResult;

        if (loginRequest.password() != null) {
            loginResult = authenticationProvider.authenticate(
                loginRequest.login(),
                loginRequest.password());
        } else {
            loginResult = authenticationProvider.authenticate(
                loginRequest.login());
        }

        if (loginResult.isFailure()) {
            return HttpUtils.responseError(loginResult.getError(), commonMapper);
        }

        UserModel authUser = null;

        if (loginRequest.password() != null) {
            authUser = userService.getByLogin(loginRequest.login()).getData();
        } else {
            authUser = userService.getById(Long.parseLong(loginRequest.login())).getData();
        }

        ResultT<String> accessTokenResult = accessTokenService.generateToken(authUser.getId());

        if (accessTokenResult.isFailure()) {
            return HttpUtils.responseError(accessTokenResult.getError(), commonMapper);
        }

        ResultT<Boolean> installRefreshTokenResult = refreshTokenCookieProvider.handleInstallRefreshToken(authUser.getId(), servletRequest, servletResponse);

        if (installRefreshTokenResult.isFailure()) {
            return HttpUtils.responseError(installRefreshTokenResult.getError(), commonMapper);
        }

        TokensResponse tokensResponse = new TokensResponse(
            accessTokenResult.getData()
        );

        return ResponseEntity.ok(tokensResponse);
    }

    @PostMapping("/refresh-tokens")
    public ResponseEntity<?> refreshTokens(HttpServletRequest servletRequest, HttpServletResponse servletResponse) {
        ResultT<RefreshSessionModel> existsRefreshTokenResult = refreshTokenCookieProvider
                .handleReinstallRefreshToken(servletRequest, servletResponse);

        if (existsRefreshTokenResult.isFailure()) {
            return HttpUtils.responseError(existsRefreshTokenResult.getError(), commonMapper);
        }

        RefreshSessionModel existsRefreshToken = existsRefreshTokenResult.getData();

        ResultT<String> generatedAccessTokenResult = accessTokenService
                .generateToken(existsRefreshToken.getUserId());

        if (generatedAccessTokenResult.isFailure()) {
            return HttpUtils.responseError(generatedAccessTokenResult.getError(), commonMapper);
        }

        TokensResponse tokensResponse = new TokensResponse(
            generatedAccessTokenResult.getData()
        );

        return ResponseEntity.ok(tokensResponse);
    }
}
