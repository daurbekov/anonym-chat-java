package anonym.chat.web.contracts.message;

public record CreateNewMessageRequest(
    String body
) { }
