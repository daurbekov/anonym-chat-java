package anonym.chat.web.configs;

import anonym.chat.web.abstractions.ConfigSplitter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class JwtConfig {
    private final ConfigSplitter configSplitter;

    public JwtConfig(ConfigSplitter configSplitter) {
        this.configSplitter = configSplitter;
    }

    @Value("${security.access-token.secret-key}")
    private String secretKey;

    @Value("${security.access-token.issuer}")
    private String issuer;

    @Value("${security.access-token.audiences}")
    private String audience;

    @Value("${security.access-token.expiration}")
    private String minutesExpiration;

    public String getSecretKey() {
        return this.secretKey;
    }

    public String getIssuer() {
        return this.issuer;
    }

    public List<String> getAudienceList() {
        return configSplitter.split(audience);
    }

    public int getMinutesExpiration() {
        return Integer.parseInt(this.minutesExpiration);
    }
}
