package anonym.chat.web.contracts.auth.login;

import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Null;

public record LoginRequest(
    String login,
    String password
) { }
