package anonym.chat.web.security;

import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AccessTokenLoginProvider;
import anonym.chat.web.abstractions.auth.AuthChannelInterceptor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.NonNull;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.stereotype.Component;

@Component
public class AccessTokenChannelInterceptor implements AuthChannelInterceptor {
    private final String ACCESS_TOKEN_HEADER = "accessToken";

    private final AccessTokenLoginProvider accessTokenLoginProvider;

    public AccessTokenChannelInterceptor(
            AccessTokenLoginProvider accessTokenLoginProvider) {
        this.accessTokenLoginProvider = accessTokenLoginProvider;
    }

    // TODO: ANONYMCHAT-44
    @Override
    public Message<?> preSend(@NonNull Message<?> message, @NonNull MessageChannel channel) {
        StompHeaderAccessor stompHeaderAccessor = MessageHeaderAccessor.getAccessor(message, StompHeaderAccessor.class);

        if (StompCommand.CONNECT.equals(stompHeaderAccessor.getCommand())) {
            String accessToken = stompHeaderAccessor.getFirstNativeHeader(ACCESS_TOKEN_HEADER);

            if (StringUtils.isEmpty(accessToken)) {
                throw new BadCredentialsException("Not found access token header");
            }

            ResultT<Boolean> loginResult = accessTokenLoginProvider.login(accessToken);

            if (loginResult.isFailure()) {
                throw new BadCredentialsException("Login is failed");
            }
        }

        return message;
    }
}
