package anonym.chat.web.controllers.api.room;

import anonym.chat.core.abstractions.room.RoomService;
import anonym.chat.core.abstractions.room.profile.RoomProfileService;
import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.room.CreateNewRoom;
import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.web.abstractions.auth.AuthenticatedUserProvider;
import anonym.chat.web.contracts.error.ListError;
import anonym.chat.web.contracts.room.CreateNewRoomRequest;
import anonym.chat.web.contracts.room.RoomResponse;
import anonym.chat.web.contracts.room.profile.RoomProfileWithUserFullResponse;
import anonym.chat.web.contracts.section.SectionResponse;
import anonym.chat.web.helpers.contracts.RoomHelperContract;
import anonym.chat.web.helpers.contracts.RoomProfileHelperContract;
import anonym.chat.web.mappers.CommonMapper;
import anonym.chat.web.mappers.contracts.RoomMapper;
import anonym.chat.web.mappers.contracts.RoomProfileMapper;
import anonym.chat.web.utils.HttpUtils;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

// TODO: переделать connect и disconnect в /{id}/connections

@RestController
@RequestMapping(
    value = "/api/rooms",
    produces = MediaType.APPLICATION_JSON_VALUE)
public class RoomController {
    private final RoomService roomService;

    private final AuthenticatedUserProvider authenticatedUserProvider;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final CommonMapper commonMapper;
    private final RoomMapper roomMapper;
    private final RoomProfileService roomProfileService;
    private final RoomProfileHelperContract roomProfileHelperContract;
    private final RoomProfileMapper roomProfileMapper;
    private final RoomHelperContract roomHelperContract;


    public RoomController(
            AuthenticatedUserProvider authenticatedUserProvider,
            RoomService roomService,
            SimpMessagingTemplate simpMessagingTemplate,
            CommonMapper commonMapper,
            RoomMapper roomMapper,
            RoomProfileService roomProfileService,
            RoomProfileHelperContract roomProfileHelperContract,
            RoomProfileMapper roomProfileMapper,
            RoomHelperContract roomHelperContract) {
        this.authenticatedUserProvider = authenticatedUserProvider;
        this.roomService = roomService;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.commonMapper = commonMapper;
        this.roomMapper = roomMapper;
        this.roomProfileService = roomProfileService;
        this.roomProfileHelperContract = roomProfileHelperContract;
        this.roomProfileMapper = roomProfileMapper;
        this.roomHelperContract = roomHelperContract;
    }

    @ApiResponse(responseCode = "403")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = RoomResponse.class)))
    @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(oneOf = { ListError.class, Error.class })))
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> create(@RequestBody CreateNewRoomRequest request) {
        Optional<UserModel> authUser = authenticatedUserProvider.get();

        if (authUser.isEmpty()) {
            return ResponseEntity.internalServerError().build();
        }

        ResultT<RoomModel> createRoomResult = roomService.add(new CreateNewRoom(
            authUser.get().getId(),
            request.name(),
            request.description()
        ));

        if (createRoomResult.isFailure()) {
            return HttpUtils.responseError(createRoomResult.getError(), commonMapper);
        }

        RoomResponse roomResponse = roomHelperContract.toRoomResponseFromRoomModel(createRoomResult.getData());

        return ResponseEntity.ok(roomResponse);
    }

    /**
     * TODO: Поменять контракт для респонса. Должно отображаться кол-во подключенных
     */
    @ApiResponse(responseCode = "403")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = RoomResponse.class)))
    @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(oneOf = { ListError.class, Error.class })))
    @GetMapping
    public ResponseEntity<?> getPage(@RequestParam("page") Long page) {
        ResultT<SectionCollection<RoomModel>> pageResult =  roomService.getAllPage(page);

        if (pageResult.isFailure()) {
            return HttpUtils.responseError(pageResult.getError(), commonMapper);
        }

        SectionResponse<RoomResponse> response = roomHelperContract.toSectionResponseRoomsFromSection(pageResult.getData());

        return ResponseEntity.ok(response);
    }

    @ApiResponse(responseCode = "403")
    @ApiResponse(responseCode = "200", content = @Content(schema = @Schema(implementation = RoomResponse.class)))
    @ApiResponse(responseCode = "400", content = @Content(schema = @Schema(oneOf = { ListError.class, Error.class })))
    @ApiResponse(responseCode = "404", content = @Content(schema = @Schema(implementation = Error.class)))
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable("id") long roomId) {
        ResultT<RoomModel> existsRoomResult = roomService.getById(roomId);

        if (existsRoomResult.isFailure()) {
            return HttpUtils.responseError(existsRoomResult.getError(), commonMapper);
        }

        RoomResponse roomResponse = roomHelperContract.toRoomResponseFromRoomModel(existsRoomResult.getData());

        return ResponseEntity.ok(roomResponse);
    }

    @GetMapping("/my")
    public ResponseEntity<?> getUsersRooms(@RequestParam long page) {
        UserModel authUser = authenticatedUserProvider.get().get();

        ResultT<SectionCollection<RoomModel>> sectionResult = roomService.getAllByUserIdInPage(authUser.getId(), page);

        if (sectionResult.isFailure()) {
            return HttpUtils.responseError(sectionResult.getError(), commonMapper);
        }

        SectionResponse<RoomResponse> response = roomHelperContract
                .toSectionResponseRoomsFromSection(sectionResult.getData());

        return ResponseEntity.ok(response);
    }

    @PostMapping("/{id}/connections")
    public ResponseEntity<?> connectToRoom(@PathVariable("id") long roomId) {
        UserModel authUser = authenticatedUserProvider.get().get();

        ResultT<Boolean> connectResult = roomService.connectUserToRoom(authUser.getId(), roomId);

        if (connectResult.isFailure()) {
            return HttpUtils.responseError(connectResult.getError(), commonMapper);
        }

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}/connections")
    public ResponseEntity<?> disconnectFromRoom(@PathVariable("id") long roomId) {
        UserModel authUser = authenticatedUserProvider.get().get();

        ResultT<Boolean> disconnectResult = roomService.disconnectUserFromRoom(authUser.getId(), roomId);

        if (disconnectResult.isFailure()) {
            return HttpUtils.responseError(disconnectResult.getError(), commonMapper);
        }

        return ResponseEntity.ok().build();
    }

    @GetMapping("/{id}/connections")
    public ResponseEntity<?> getConnectedUsersToRoom(@PathVariable("id") long roomId, @RequestParam long section) {
        ResultT<SectionCollection<UserModel>> connectedUsersResult = roomService
                .getAllConnectedUsersInRoomSection(roomId, section);

        if (connectedUsersResult.isFailure()) {
            return HttpUtils.responseError(connectedUsersResult.getError(), commonMapper);
        }

        SectionResponse<RoomProfileWithUserFullResponse> response = roomProfileHelperContract
                .generateSectionRoomProfileWithUserFullResponse(connectedUsersResult.getData(), roomId);

        return ResponseEntity.ok(response);
    }
}
