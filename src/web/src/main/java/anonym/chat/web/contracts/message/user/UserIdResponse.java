package anonym.chat.web.contracts.message.user;

public record UserIdResponse(
    long userId
) { }
