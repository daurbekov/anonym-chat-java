package anonym.chat.web.security;

import anonym.chat.application.validators.user.UserIdValidator;
import anonym.chat.core.abstractions.providers.DateTimeProvider;
import anonym.chat.core.abstractions.Validator;
import anonym.chat.core.abstractions.token.AccessTokenService;
import anonym.chat.core.abstractions.user.UserService;
import anonym.chat.core.constants.errors.AccessTokenErrorsConstants;
import anonym.chat.core.models.token.Claim;
import anonym.chat.core.models.user.UserModel;
import anonym.chat.core.utils.claim.ClaimUtils;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;
import anonym.chat.core.utils.validator.ValidationResult;
import anonym.chat.core.utils.validator.ValidatorUtils;
import anonym.chat.web.configs.JwtConfig;
import anonym.chat.core.utils.DateAndTimeUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.io.Encoders;
import io.jsonwebtoken.security.Keys;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.crypto.SecretKey;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;
import java.util.Optional;

@Service
public final class AccessTokenServiceImpl implements AccessTokenService {
    private final JwtConfig jwtConfig;

    private final DateTimeProvider dateTimeProvider;
    private final UserService userService;
    private final Validator validator;

    public AccessTokenServiceImpl(
            JwtConfig jwtConfig,
            DateTimeProvider dateTimeProvider,
            UserService userService,
            Validator validator) {
        this.jwtConfig = jwtConfig;
        this.dateTimeProvider = dateTimeProvider;
        this.userService = userService;
        this.validator = validator;
    }

    @Override
    public ResultT<String> generateToken(long userId) {
        UserIdValidator userIdValidationModel = new UserIdValidator(userId);
        ValidationResult validationResult = validator.validate(userIdValidationModel);

        if (!validationResult.isValid()) {
            return ValidatorUtils.validationErrorFromErrors(validationResult.getErrors());
        }

        ResultT<UserModel> existsUserResult= userService.getById(userId);

        if (existsUserResult.isFailure()) {
            return ResultUtils.fromTSingleError(existsUserResult.getError());
        }

        UserModel existsUser = existsUserResult.getData();

        Map<String, ?> claims = Map.of(
                Claims.SUBJECT, StringUtils.isEmpty(existsUser.getLogin()) ? existsUser.getId() : existsUser.getLogin(),
                "id", existsUser.getId(),
                "role", existsUser.getRole().getValue()
        );

        anonym.chat.core.utils.claim.Claims claimsObject = ClaimUtils.getFrom(claims);

        String accessToken = generateToken(claimsObject);

        return ResultUtils.fromTData(accessToken);
    }

    @Override
    public ResultT<UserModel> getUserByToken(String token) {
        ResultT<Boolean> isValidTokenResult = checkTokenValid(token);

        if (isValidTokenResult.isFailure()) {
            return ResultUtils.fromTSingleError(isValidTokenResult.getError());
        }

        ResultT<Claim<Long>> userIdClaimResult = extractClaim(token, "id");

        if (userIdClaimResult.isFailure()) {
            return ResultUtils.fromTSingleError(userIdClaimResult.getError());
        }

        Claim<Long> userIdClaim = userIdClaimResult.getData();

        return userService.getById(userIdClaim.value());
    }

    @Override
    public <TClaim> ResultT<Claim<TClaim>> extractClaim(String token, String key) {
        ResultT<anonym.chat.core.utils.claim.Claims> claimsResult = extractAllClaims(token);

        if (claimsResult.isFailure()) {
            return ResultUtils.fromTSingleError(claimsResult.getError());
        }

        anonym.chat.core.utils.claim.Claims claims = claimsResult.getData();

        Optional<Claim<TClaim>> targetClaim = claims.getByKey(key);

        return targetClaim.map(ResultUtils::fromTData).orElseGet(() -> ResultUtils.fromTSingleError(
            AccessTokenErrorsConstants.NOT_EXISTS_CLAIM_BY_KEY_TYPE,
            AccessTokenErrorsConstants.NOT_EXISTS_CLAIM_BY_KEY_TITLE,
            AccessTokenErrorsConstants.NOT_EXISTS_CLAIM_BY_KEY_STATUS,
            AccessTokenErrorsConstants.NOT_EXISTS_CLAIM_BY_KEY_DESCRIPTION
        ));
    }

    @Override
    public ResultT<anonym.chat.core.utils.claim.Claims> extractAllClaims(String token) {
        ResultT<Boolean> isTokenValidResult = checkTokenValid(token);

        if (isTokenValidResult.isFailure()) {
            return ResultUtils.fromTSingleError(isTokenValidResult.getError());
        }

        SecretKey secretKey = getSecretKey();
        Claims sourceClaims = Jwts.parser().verifyWith(secretKey).build()
                .parseSignedClaims(token)
                .getPayload();

        anonym.chat.core.utils.claim.Claims claims = ClaimUtils.getFrom(sourceClaims);

        return ResultUtils.fromTData(claims);
    }

    @Override
    public ResultT<Boolean> checkTokenValid(String token) {
        SecretKey secretKey = getSecretKey();

        try {
            Jwts.parser().verifyWith(secretKey).build().parseSignedClaims(token);
        }
        catch (ExpiredJwtException expired) {
            return ResultUtils.fromTSingleError(
                AccessTokenErrorsConstants.EXPIRED_TOKEN_TYPE,
                AccessTokenErrorsConstants.EXPIRED_TOKEN_TITLE,
                AccessTokenErrorsConstants.EXPIRED_TOKEN_STATUS,
                AccessTokenErrorsConstants.EXPIRED_TOKEN_DESCRIPTION
            );
        }
        catch (Exception other) {
            return ResultUtils.fromTSingleError(
                AccessTokenErrorsConstants.WRONG_TOKEN_TYPE,
                AccessTokenErrorsConstants.WRONG_TOKEN_TITLE,
                AccessTokenErrorsConstants.WRONG_TOKEN_STATUS,
                AccessTokenErrorsConstants.WRONG_TOKEN_DESCRIPTION
            );
        }

        return ResultUtils.fromTData(true);
    }

    private String generateToken(anonym.chat.core.utils.claim.Claims claims) {
        SecretKey secretKey = getSecretKey();
        LocalDateTime expirationDateTime = dateTimeProvider.now().plusMinutes(jwtConfig.getMinutesExpiration());
        Date expirationDate = DateAndTimeUtils.toDate(expirationDateTime);
        Date issuedAtDate = DateAndTimeUtils.toDate(dateTimeProvider.now());

        return Jwts.builder()
                .audience().add(jwtConfig.getAudienceList()).and()
                .issuer(jwtConfig.getIssuer())
                .issuedAt(issuedAtDate)
                .expiration(expirationDate)
                .claims(ClaimUtils.toHashMap(claims))
                .signWith(secretKey)
                .compact();
    }

    private SecretKey getSecretKey() {
        String encodedString = Encoders.BASE64.encode(jwtConfig.getSecretKey().getBytes());
        byte[] decodedBytes = Decoders.BASE64.decode(encodedString);
        return Keys.hmacShaKeyFor(decodedBytes);
    }
}
