plugins {
    id("java")
    alias(libs.plugins.spring.boot)
}

group = "anonym.chat.web"
version = "0.0.1"

dependencies {
    implementation(platform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))

    implementation(project(":src:core"))
    implementation(project(":src:application"))
    implementation(project(":src:infrastructure"))

    testImplementation(libs.testng)
    testImplementation(libs.spring.boot.starter.test)
    testImplementation(libs.spring.security.test)
    testImplementation(libs.spring.boot.starter.data.jpa)
    testImplementation(libs.support.tests.faker)

    implementation(libs.spring.boot.starter.swagger)
    implementation(libs.spring.boot.starter.web)
    implementation(libs.spring.boot.starter.security)
    implementation(libs.spring.boot.starter.websocket)
    implementation(libs.support.apache.tika)
    implementation(libs.support.jwt.api)
    implementation(libs.support.jwt.impl)
    implementation(libs.support.jwt.jackson)

    implementation(libs.mapstruct.mapper)
    annotationProcessor(libs.mapstruct.annotation.processor)
}

tasks.test {
    useTestNG()
}