package anonym.chat.core.models.room.profile;

public record CreateNewRoomProfile(
    long userId,
    Long avatarId,
    String nickname
) { }
