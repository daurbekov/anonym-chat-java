package anonym.chat.core.utils.validator;

import java.util.List;
import java.util.Map;

public interface ValidationResult {
    boolean isValid();
    Map<String, List<String>> getErrors();
}
