package anonym.chat.core.models.message.user;

import anonym.chat.core.models.message.BaseMessage;

public class UserMessageModel extends BaseMessage {
    private long userId;


    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
