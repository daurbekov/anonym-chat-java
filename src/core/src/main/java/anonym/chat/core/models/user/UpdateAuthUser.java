package anonym.chat.core.models.user;

public record UpdateAuthUser(
    long userId,
    Role role,
    UserAuth userAuth
) { }
