package anonym.chat.core.models.message.room;

public record CreateNewPrivateRoomMessage(
    long userId,
    long roomId,
    long destinationUserId,
    String body
) { }
