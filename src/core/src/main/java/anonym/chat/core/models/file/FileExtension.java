package anonym.chat.core.models.file;

public enum FileExtension {
    PNG("png"),
    JPG("jpg"),
    JPEG("jpeg"),
    GIF("gif"),
    MP4("mp4");


    private final String value;

    FileExtension(final String newValue) {
        this.value = newValue;
    }

    public String getValue() {
        return value;
    }

    public static FileExtension fromExtension(String extension) {
        for (FileExtension fileExtension : FileExtension.values()) {
            if (fileExtension.getValue().equals(extension)) {
                return fileExtension;
            }
        }

        return null;
    }
}
