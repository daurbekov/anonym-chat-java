package anonym.chat.core.constants.errors;

public final class RoomErrorsConstants {
    public static final String TYPE = CommonErrorsConstants.TYPE + "/rooms";

    public static final String CONNECT_IS_ALREADY_EXISTS_TYPE = TYPE + "/connect-is-already-exists";
    public static final String CONNECT_IS_ALREADY_EXISTS_TITLE = "Подключение в комнату уже существует";
    public static final int CONNECT_IS_ALREADY_EXISTS_STATUS = 400;
    public static final String CONNECT_IS_ALREADY_EXISTS_DESCRIPTION = CommonErrorsConstants.INCORRECT_REQUEST_DESCRIPTION;

    public static final String CONNECT_IS_NOT_EXISTS_TYPE = TYPE + "/connect-is-not-exists";
    public static final String CONNECT_IS_NOT_EXISTS_TITLE = "Подключения в комнату не существует";
    public static final int CONNECT_IS_NOT_EXISTS_STATUS = 400;
    public static final String CONNECT_IS_NOT_EXISTS_DESCRIPTION = CommonErrorsConstants.INCORRECT_REQUEST_DESCRIPTION;

    public static final String NOT_SELECTED_ROOM_PROFILE_FOR_ROOM_TYPE = TYPE + "/not-selected-room-profile-for-room";
    public static final String NOT_SELECTED_ROOM_PROFILE_FOR_ROOM_TITLE = "Не был выбран профиль для выбранной комнаты";
    public static final int NOT_SELECTED_ROOM_PROFILE_FOR_ROOM_STATUS = 400;
    public static final String NOT_SELECTED_ROOM_PROFILE_FOR_ROOM_DESCRIPTION = "Попробуйте установить профиль комнаты";
}
