package anonym.chat.core.models.room.profile;

public record IsInstalledToRoom(
    long roomId,
    long roomProfileId,
    long userId
) { }
