package anonym.chat.core.utils.token;

public record RefreshTokenConfig(
    int expiresInDays
) { }
