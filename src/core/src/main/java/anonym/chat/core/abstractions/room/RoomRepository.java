package anonym.chat.core.abstractions.room;

import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.room.RoomModel;
import anonym.chat.core.models.user.UserModel;

import java.util.List;

public interface RoomRepository {
    RoomModel add(RoomModel roomModel);
    RoomModel getById(long id);
    List<RoomModel> getAllInSection(long offset, long count);
    long getTotal();
    List<RoomModel> getAllByUserIdInSection(long userId, long offset, long count);
    long getTotalByUserId(long userId);
    int getConnectedUsersCountInRoom(long roomId);
    List<UserModel> getAllConnectedUsersInRoomSection(long roomId, long offset, long count);
    boolean connectUserToRoom(long userId, long roomId);
    long deleteById(long id);
    boolean isConnectedToRoom(long userId, long roomId);
    boolean disconnectUserFromRoom(long userId, long roomId);
}
