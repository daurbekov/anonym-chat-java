package anonym.chat.core.utils.validator;

import anonym.chat.core.constants.errors.CommonErrorsConstants;
import anonym.chat.core.utils.result.ResultError;
import anonym.chat.core.utils.result.ResultT;
import anonym.chat.core.utils.result.ResultUtils;

import java.util.List;
import java.util.Map;

public final class ValidatorUtils {
    public static ValidationResult invalid(Map<String, List<String>> errors) {
        return new ValidatorResultImpl(false, errors);
    }

    public static ValidationResult valid() {
        return new ValidatorResultImpl(true, null);
    }

    public static <TData> ResultT<TData> validationErrorFromErrors(Map<String, List<String>> errors) {
        return ResultUtils.fromTError(
            CommonErrorsConstants.VALIDATION_TYPE,
            CommonErrorsConstants.VALIDATION_TITLE,
            CommonErrorsConstants.VALIDATION_STATUS,
            errors
        );
    }
}
