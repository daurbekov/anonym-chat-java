package anonym.chat.core.utils.claim;

import anonym.chat.core.models.token.Claim;

import java.util.Map;
import java.util.stream.Collectors;

public final class ClaimUtils {
    public static Claims getFrom(Map<String, ?> map) {
        return new ClaimsImpl(map);
    }

    public static Map<String, ?> toHashMap(Claims source) {
        return source.getAll().stream().collect(Collectors.toMap(Claim::key, Claim::value));
    }
}
