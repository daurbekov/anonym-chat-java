package anonym.chat.core.models;

import java.util.List;

public record SectionCollection<TItem>(
        long section,
        long lastSection,
        long perSection,
        long total,
        List<TItem> items
) { }
