package anonym.chat.core.models.token.refresh;

public record CheckTokenIsValid(
    String refreshToken
) { }
