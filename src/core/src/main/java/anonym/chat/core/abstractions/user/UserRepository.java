package anonym.chat.core.abstractions.user;

import anonym.chat.core.models.user.UserModel;


public interface UserRepository {
    UserModel add(UserModel userModel);
    UserModel addWithPassword(UserModel userModel, String password);
    UserModel getById(long id);
    UserModel getByLogin(String login);
    String getPasswordHashById(long id);
    UserModel update(UserModel userModel);
    UserModel updateWithPassword(UserModel userModel, String password);
}
