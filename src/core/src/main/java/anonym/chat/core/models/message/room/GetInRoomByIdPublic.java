package anonym.chat.core.models.message.room;

public record GetInRoomByIdPublic(
    long roomId,
    long messageId
) { }
