package anonym.chat.core.utils.result;

public interface Result<TData, TError> {
    TData getData();
    TError getError();

    boolean isSuccess();
    boolean isFailure();
}
