package anonym.chat.core.abstractions.providers.config;

import anonym.chat.core.utils.paginations.PaginationConfig;

public interface PaginationConfigProvider extends ConfigProvider<PaginationConfig> { }
