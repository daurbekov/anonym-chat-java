package anonym.chat.core.abstractions.avatar;

import anonym.chat.core.models.SectionCollection;
import anonym.chat.core.models.avatar.AvatarModel;
import anonym.chat.core.models.avatar.CreateNewAvatar;
import anonym.chat.core.models.avatar.GetAllOfUserSection;
import anonym.chat.core.utils.result.ResultT;

public interface AvatarService {
    ResultT<AvatarModel> add(CreateNewAvatar createNewAvatar);
    ResultT<AvatarModel> getOfUserById(long id, long userId);
    ResultT<AvatarModel> getDefaultAvatar();
    ResultT<SectionCollection<AvatarModel>> getAllOfUserSection(GetAllOfUserSection get);
}
