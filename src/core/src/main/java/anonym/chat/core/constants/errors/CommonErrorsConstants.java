package anonym.chat.core.constants.errors;

public final class CommonErrorsConstants {
    public static String TYPE = "/errors";

    public static String INCORRECT_REQUEST_DESCRIPTION = "Некорректный запрос";

    public static String DESCRIPTION_VALID = "Попробуйте указать верные параметры";

    public static String VALIDATION_TYPE = TYPE + "/validation";
    public static String VALIDATION_TITLE = "Ошибка валидации";
    public static int VALIDATION_STATUS = 400;

    public static String AUTH_TYPE = TYPE + "/auth";
    public static String AUTH_TITLE = "Ошибка авторизации";
    public static int AUTH_STATUS = 401;
    public static String AUTH_DESCRIPTION_INVALID_PASSWORD_OR_LOGIN = "Неверный логин или пароль";

    public static String INTERNAL_SERVER_ERROR_TYPE = TYPE + "/server-error";
    public static String INTERNAL_SERVER_ERROR_TITLE = "Неизвестная ошибка!";
    public static int INTERNAL_SERVER_ERROR_STATUS = 500;
    public static String INTERNAL_SERVER_ERROR_DESCRIPTION = "Возможно, вам стоит сообщить об этом в обратной связи.";
}
