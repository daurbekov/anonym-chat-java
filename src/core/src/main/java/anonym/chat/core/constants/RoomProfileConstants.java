package anonym.chat.core.constants;

public final class RoomProfileConstants {
    public static final int NICKNAME_MIN_LENGTH = 5;
    public static final int NICKNAME_MAX_LENGTH = 255;
}
