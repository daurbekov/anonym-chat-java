package anonym.chat.core.abstractions.avatar;

import anonym.chat.core.models.avatar.AvatarModel;

import java.util.List;

public interface AvatarRepository {
    AvatarModel add(long userId, long fileId);
    AvatarModel getOfUserById(long id, long userId);
    List<AvatarModel> getAllOfUserSection(long userId, long offset, long count);
    long getTotalOfUser(long userId);
}
