package anonym.chat.core.utils;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalAccessor;
import java.util.Date;

public final class DateAndTimeUtils {
    public static Date toDate(LocalDateTime source) {
        return Date.from(source.atZone(ZoneId.systemDefault())
                .toInstant());
    }

    public static LocalDateTime toLocalDateTime(Date source) {
        return source.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
}
