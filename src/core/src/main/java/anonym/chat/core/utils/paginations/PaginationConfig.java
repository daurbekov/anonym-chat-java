package anonym.chat.core.utils.paginations;

public record PaginationConfig(
    long perPageDefault
) { }
