package anonym.chat.core.utils.claim;

import anonym.chat.core.models.token.Claim;
import anonym.chat.core.utils.DateAndTimeUtils;

import java.security.Key;
import java.util.*;

final class ClaimsImpl implements Claims {
    private final List<Claim<?>> values;

    ClaimsImpl(Map<String, ?> initialValues) {
        this.values = new ArrayList<>();

        for (Map.Entry<String, ?> value : initialValues.entrySet()) {
            if (value.getValue() instanceof Date date) {
                values.add(new Claim<>(value.getKey(), DateAndTimeUtils.toLocalDateTime(date)));
            } else if (value.getKey().equals("sub")) {
                values.add(new Claim<>(value.getKey(), String.valueOf(value.getValue())));
            } else if (List.of("id").contains(value.getKey())) {
                values.add(new Claim<>(value.getKey(), Long.parseLong(String.valueOf(value.getValue()))));
            }
            else {
                values.add(new Claim<>(value.getKey(), value.getValue()));
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public <TClaim> Optional<Claim<TClaim>> getByKey(String key) {
        try {
            return values.stream().filter(s -> s.key().equals(key)).map(e -> (Claim<TClaim>)e).findFirst();
        } catch(Exception e) {
            return Optional.empty();
        }
    }

    @Override
    public List<Claim<?>> getAll() {
        return values;
    }
}
