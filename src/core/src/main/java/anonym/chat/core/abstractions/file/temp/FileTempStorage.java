package anonym.chat.core.abstractions.file.temp;

import anonym.chat.core.models.file.temp.FileTempModel;

import java.io.File;
import java.io.InputStream;
import java.util.Optional;

public interface FileTempStorage {
    Optional<FileTempModel> saveTempFileByPath(String path);
    Optional<FileTempModel> saveTempFileByStream(InputStream stream);
    boolean deleteTempFile(File toDelete);
}
